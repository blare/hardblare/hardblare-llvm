//===- HardBlare.cpp - LLVM-IR Level Passes for HardBlare -----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under ......
// License. See LICENSE.TXT for details.
//
// Mounir NASR ALLAH <mounir.nasrallah@centralsupelec.fr>
//
//===----------------------------------------------------------------------===//
//
// This file implements the HardBlare LLVM-IR Level Passes.
// In order to targeting the code for the HardBlare plateform, we need
// those features:
//   - Replace every "select" instructions to conditional branches
//   -
//
//
//
//===----------------------------------------------------------------------===//

//#include "llvm/CodeGen/HardBlareCodeGenIRPass.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;

#define DEBUG_TYPE "HardBlare-CodeGen-IR-Pass"


namespace {
  struct HardBlareCodeGenIRPass: public FunctionPass {
  public:

    static char ID;

    HardBlareCodeGenIRPass() : FunctionPass(ID) {}

    bool runOnFunction(Function &F) override;

  };
}



char HardBlareCodeGenIRPass::ID = 0;

char &llvm::HardBlareCodeGenIRPassID = HardBlareCodeGenIRPass::ID;

FunctionPass *llvm::createHardBlareCodeGenIRPass() { return new HardBlareCodeGenIRPass(); }


bool HardBlareCodeGenIRPass::runOnFunction(Function &F) {

  Module *M = F.getParent();

  M->getOrInsertGlobal("__hardblare_instr_addr", Type::getInt32Ty(M->getContext()));

  bool Modified = true;

  return Modified;
}
