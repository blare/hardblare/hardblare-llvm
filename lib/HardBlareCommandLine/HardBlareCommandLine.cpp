//===---------------------------- HardBlareCommandLine.h ---------------------------===//
//
//
// HardBlare Project
//
// This file permit to access to the '-hardblare' command
// line option
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//

#include <stdlib.h>
#include <unistd.h>
#include "llvm/Support/CommandLine.h"

using namespace llvm;

bool HardBlareAnnotFlag = false;

bool HardBlareInstrFlag = false;

bool HardBlareSelectPassFlag = false;

// HardBlare Annot
static cl::opt<bool, true>
HardBlareAnnotOpt("hardblare-annot", cl::desc("Enable the HardBlare Annotations process"),
      cl::Hidden, cl::location(HardBlareAnnotFlag));


// HardBlare Instr
static cl::opt<bool, true>
HardBlareInstrOpt("hardblare-instr", cl::desc("Enable the HardBlare Instrumentation process"),
      cl::Hidden, cl::location(HardBlareInstrFlag));


// HardBlare Select Pass
static cl::opt<bool, true>
HardBlareSelectPassOpt("hardblare-select-pass", cl::desc("Enable the HardBlare the select pass process"),
      cl::Hidden, cl::location(HardBlareSelectPassFlag));


