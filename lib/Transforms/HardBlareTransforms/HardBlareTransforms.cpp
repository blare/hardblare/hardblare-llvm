//===- HardBlare.cpp - LLVM-IR Level Passes for HardBlare -----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under ......
// License. See LICENSE.TXT for details.
//
// Mounir NASR ALLAH <mounir.nasrallah@centralsupelec.fr>
//
//===----------------------------------------------------------------------===//
//
// This file implements the HardBlare LLVM-IR Level Passes.
// In order to targeting the code for the HardBlare plateform, we need
// those features:
//   - Replace every "select" instructions to conditional branches
//   -
//
//
//
//===----------------------------------------------------------------------===//

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "LowerSelect.h"
using namespace llvm;

#define DEBUG_TYPE "HardBlare-LLVM-IR"


namespace {
  struct HardBlareTransformsIRPass: public ModulePass {
    static char ID;
    HardBlareTransformsIRPass() : ModulePass(ID) {}


    bool runOnModule(Module &M) {

      bool Modified = true;

      return Modified;
    }

  };

}


//char HardBlareTransformsIRPass::ID = 0;
//static RegisterPass<HardBlareTransformsIRPass> X("hardblare-ir", "HardBlare LLVM-IR level Passes");

//Pass *llvm::createHardBlareTransformsIRPass() { return new HardBlareTransformsIRPass(); }
