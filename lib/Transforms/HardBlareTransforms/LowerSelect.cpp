//===- HardBlare.cpp - LLVM-IR Level Passes for HardBlare -----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under ......
// License. See LICENSE.TXT for details.
//
// Mounir NASR ALLAH <mounir.nasrallah@centralsupelec.fr>
//
//===----------------------------------------------------------------------===//
// Remove "select" instruction and replace them by two basic blocks.
// The ‘select‘ instruction is used to choose one value based on a condition,
// without IR-level branching.
//
//
//         BB                                       BB
//    _____________                  _______________________________
//   |     ...    |                  |                              |
//   |     ...    |                  |  b cond, BBTrueCase, BBRest  |
//   |     ...    |                  |______________________________|
//   |     ...    |                          |           |
//   |     ...    |                          |           |
//   |     ...    |                          |           |      BBTrueCase
//   |    select  |       ====>              |           |    _____________
//   |     ...    |                          |           +-> |            |
//   |     ...    |                          |              /|  b BBRest  |
//   |     ...    |                          V   BBRest    / |____________|
//   |     ...    |                          ____________ L
//   |     ...    |                         |     Phi    |
//   |____________|                         |____________|
//
//
//
//
// Case 1 : We use the same conditionnal code into more than one select instruction
//         -> Schedule instruction in order to have those instructions into the
//            same basic block
//
// Case 2 : The select instruction is used ONLY for selecting a pointing address
//          where we branch later.
//            -> Don't need to deal with it (we will receive the PTM Trace of
//               the address which is selected)
//
// Case 3 : The select instruction who has the PC register as destination.
//         -> Don't need to deal with it (we will receive the PTM Trace of
//            the address which is selected)
//
//===----------------------------------------------------------------------===//


#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "LowerSelect.h"

using namespace llvm;


#define DEBUG_TYPE "HardBlare-LowerSelect"




// Return true if for two "select" instructions, we have the same
// conditional code
bool LowerSelect::isTheSameConditionnalCode(SelectInst* a, SelectInst* b){

  Value *a_CC = a->getCondition();
  Value *b_CC = b->getCondition();

  if(a_CC == b_CC){
    return true;
  }
  else{
    return false;
  }

}


bool LowerSelect::RemoveSelect(BasicBlock &BB){

    // We track if the passe modified the code or not
    bool Modified = false;

    errs() << "Enter to RemoveSelect \n";

    // On each instructions
    for (BasicBlock::iterator I = BB.begin(), E = BB.end(); I != E; I++) {

      errs() << "instruction\n";
      errs() << I->getOpcode();

      // If the instruction is a "select"
      if (SelectInst *I_select = dyn_cast<SelectInst>(I)) {

	errs() << "We find a SELECT instruction\n";

	// Split the basic block into two basic blocks at I_select. All instructions BEFORE
	// the specified iterator stay as part of the original basic block, an unconditional
	// branch is added to the original BB, and the rest of the instructions in the BB are
	// moved to the new BB, including the old terminator. The newly formed BasicBlock is
	// returned. This function invalidates the specified iterator.
	BasicBlock *BBRest = BB.splitBasicBlock(I, BB.getName()+".select_hardblare");

	// We create a new Basic Block, which is contained into the function that contain BB, and just before BBRest
	BasicBlock *BBTrueCase = BasicBlock::Create(BB.getContext(), BB.getName()+".select_true_hardblare", BB.getParent(), BBRest);

	// We add an unconditional branch instruction at the end of the BB_TrueCase Basic Block ('br BBRest')
	BranchInst* BBTrueCaseToBBRest = BranchInst::Create(BBRest, BBTrueCase);

	// ...
	BB.getInstList().erase(BB.getTerminator());

	// Add a branch instruction at the end of the BB Basic Block
	// br "I_select->getCondition()", "BBTrueCase", "BBRest"
	BranchInst::Create(BBTrueCase, BBRest, I_select->getCondition(), &BB);

	// We create a Phi Node and insert it at the begining of the BBTrueCase Basic Block.
	PHINode *Phi = PHINode::Create(I_select->getType(), 2, "", BBRest->getFirstNonPHI());

	// The Phi Node is attainable via NewTrue and BB Basic Blocks
	Phi->addIncoming(I_select->getTrueValue(), BBTrueCase);
	Phi->addIncoming(I_select->getFalseValue(), &BB);

        // Move Values into the select instruction into the respective basic block
	if(Instruction *TrueInstruction = dyn_cast<Instruction>(I_select->getTrueValue())){
	  TrueInstruction->moveBefore(BBTrueCaseToBBRest);
	  //	  BBTrueCase->getInstList().push_front(TrueInstruction);
	}

	// Use the PHI instead of the select.
	I_select->replaceAllUsesWith(Phi);
        BBRest->getInstList().erase(I_select);

	Modified = true;
	break;
      }
    }

    return Modified;

  }


bool LowerSelect::run(BasicBlock &BB) {

      bool Modified = false;

      // We turn every "select" instructions to conditional branches
      return (LowerSelect::RemoveSelect(BB));

    }



