//===- HardBlare.cpp - LLVM-IR Level Passes for HardBlare -----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under ......
// License. See LICENSE.TXT for details.
//
// Mounir NASR ALLAH <mounir.nasrallah@centralsupelec.fr>
//
//===----------------------------------------------------------------------===//
// Remove "select" instruction and replace them by two basic blocks.
// The ‘select‘ instruction is used to choose one value based on a condition,
// without IR-level branching.
//
//
//         BB                                       BB
//    _____________                  _______________________________
//   |     ...    |                  |                              |
//   |     ...    |                  |  b cond, BBTrueCase, BBRest  |
//   |     ...    |                  |______________________________|
//   |     ...    |                          |           |
//   |     ...    |                          |           |
//   |     ...    |                          |           |      BBTrueCase
//   |    select  |       ====>              |           |    _____________
//   |     ...    |                          |           +-> |            |
//   |     ...    |                          |              /|  b BBRest  |
//   |     ...    |                          V   BBRest    / |____________|
//   |     ...    |                          ____________ L
//   |     ...    |                         |     Phi    |
//   |____________|                         |____________|
//
//
//
//
// Case 1 : We use the same conditionnal code into more than one select instruction
//         -> Schedule instruction in order to have those instructions into the
//            same basic block
//
// Case 2 : The select instruction is used ONLY for selecting a pointing address
//          where we branch later.
//            -> Don't need to deal with it (we will receive the PTM Trace of
//               the address which is selected)
//
// Case 3 : The select instruction who has the PC register as destination.
//         -> Don't need to deal with it (we will receive the PTM Trace of
//            the address which is selected)
//
//===----------------------------------------------------------------------===//


#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;

class LowerSelect {

private:

  // Return true if for two "select" instructions, we have the same
  // conditional code
  static bool isTheSameConditionnalCode(SelectInst* a, SelectInst* b);

  static bool RemoveSelect(BasicBlock &BB);

public:

  static bool run(BasicBlock &BB);

};
