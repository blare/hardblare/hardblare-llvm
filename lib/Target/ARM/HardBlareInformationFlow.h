//===---------------------------- Instruction.h ---------------------------===//
//
//
// HardBlare Project
//
// This file describe ....
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//


#ifndef LLVM_HARDBLARE_INFORMATION_FLOW_H
#define LLVM_HARDBLARE_INFORMATION_FLOW_H

#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/MC/MCContext.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCObjectFileInfo.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOpcodes.h"
#include "llvm/Target/TargetSubtargetInfo.h"
#include "llvm/CodeGen/MachineOperand.h"

#include "llvm/Support/Debug.h"

#include "llvm/ADT/DenseMap.h"
#include <iterator>
#include "llvm/ADT/MapVector.h"
#include "llvm/ADT/SmallVector.h"
#include <map>
#include <vector>

#include "HardBlareRegisterFile.h"
#include "HardBlareOperand.h"
#include "HardBlareMC.h"

#include "llvm/HardBlareCommandLine/HardBlareCommandLine.h"

#include <bitset>
#include <string>
#include <iostream>
#include <climits>
#include <future>
#include <mutex>


#define DEBUG_TYPE "HardBlare-InformationFlow"

using namespace llvm;


// Annotations Low Level
// ============================================
typedef SmallVector<unsigned long, 1500> AnnotMC;



// Information Flows Helper Function
// ============================================
class HardBlareInformationFlowHelpers {

 public:

  // ============================================
  // Helper Functions
  // ============================================
  static bool needsInstrumentation(MachineBasicBlock::instr_iterator MI);

  static ARM_OPCODE_TYPE getARMOpType(MachineBasicBlock::instr_iterator MI);

  // ============================================
  // Helper Functions for SP FP updates
  // ============================================
  static std::string getARMOpTypeString(ARM_OPCODE_TYPE _type);
  static std::string getTagOpString(TAG_OP _op);

  // ============================================
  // Helper Functions for SP FP updates
  // ============================================
  static bool isUpdateSP(MachineBasicBlock::instr_iterator MI, MachineOperand& _Op);
  static bool isUpdateFP(MachineBasicBlock::instr_iterator MI, MachineOperand& _Op);

  static bool isFP(MachineOperand& _Op);
  static bool isPC(MachineOperand& _Op);
  static bool isSP(MachineOperand& _Op);
  static bool isLR(MachineOperand& _Op);

  static MachineOperand &getRd(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRn(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRm(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRHi(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRLo(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRa(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRdHi(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRdLo(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRn_wb(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRt(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRt2(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRegs(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getRegsI(const MachineBasicBlock::instr_iterator MI, unsigned i);


  // Get Addr
  static MachineOperand &getBaseAddr(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getOffsetImmAddr(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getOffsetRegAddr(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getOffsetImmAddr2(const MachineBasicBlock::instr_iterator MI);

  static MachineOperand &getShiftBase(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getShiftBase2(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getShiftImm(const MachineBasicBlock::instr_iterator MI);

  static MachineOperand &getDst(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getImm(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getImm16(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getOffset(const MachineBasicBlock::instr_iterator MI);

  static MachineOperand &getSd(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getSm(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getSn(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getDd(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getDm(const MachineBasicBlock::instr_iterator MI);
  static MachineOperand &getDn(const MachineBasicBlock::instr_iterator MI);

  static bool mayAffectControlFlow(const MachineBasicBlock::instr_iterator MI, const MCRegisterInfo &RI);

};



// Representation of a Static Information Flow
// ============================================
class HardBlareInformationFlowBase {

  // protected:

 public:
  // Operands Destionation
  std::set<HardBlareOperand, std::less<HardBlareOperand>> Outs;
  // Operands Sources
  std::set<HardBlareOperand, std::less<HardBlareOperand>> Ins;

  enum ARM_OPCODE_TYPE ARMOpType;

  TAG_OP TagOp;


  bool isOnlyReg()
  {
    bool result = true;

    for(auto Out = Outs.begin(); Out != Outs.end(); Out++)
      {
        if( (Out->isReg()) == false )
          {
            result = false;
          }
      }

    for(auto In = Ins.begin(); In != Ins.end(); In++)
      {
        if( (In->isReg()) == false )
          {
            result = false;
          }
      }

    return result;
  }


  bool isInsContainAddress()
  {

    bool result = false;

    for(auto In = Ins.begin(); In != Ins.end(); In++)
      {
        if( (In->isAddr()) == true )
          {
            result = true;
          }
      }

    return result;
  }



  bool isOutsContainAddress()
  {

    bool result = false;

    for(auto Out = Outs.begin(); Out != Outs.end(); Out++)
      {
        if( (Out->isAddr()) == true )
          {
            result = true;
          }
      }

    return result;
  }




  bool isContainTag()
  {

    bool result = false;

    for(auto Out = Outs.begin(); Out != Outs.end(); Out++)
      {
        if( (Out->isTag()) == true )
          {
            result = true;
          }
      }

    for(auto In = Ins.begin(); In != Ins.end(); In++)
      {
        if( (In->isTag()) == true )
          {
            result = true;
          }
      }

    return result;
  }



  virtual ~HardBlareInformationFlowBase()
    {
      DEBUG(errs() << "Destructor HardBlareInformationFlowBase" << "\n");
    }

  virtual bool isStatic()
  {
    return false;
  }

  virtual bool isDynamic()
  {
    return false;
  }

  virtual bool isMonitor()
  {
    return false;
  }

  virtual bool needInstrumentation()
  {
    return false;
  }

  virtual void setNeedInstrumentation(bool _v)
  {
    return;
  }

  // Information Flow emission
  virtual void emit( AnnotMC& depVec )
  {
  }

 virtual void dump()
  {

    for(auto Oit = Outs.begin(); Oit != Outs.end(); ++Oit)
      {
        (*Oit).dump();
        if(Oit != (--Outs.end()))
          {
            DEBUG( dbgs() << " | " );
          }
      }

    DEBUG( dbgs() << "  <===== ( ???? , " << HardBlareInformationFlowHelpers::getARMOpTypeString(this->ARMOpType) << ", "  <<  HardBlareInformationFlowHelpers::getTagOpString(this->TagOp) << ")=====  " );

    for(auto Iit = Ins.begin(); Iit != Ins.end(); ++Iit)
      {
        (*Iit).dump();
        if(Iit != (--Ins.end()))
          {
            DEBUG( dbgs() << " | " );
          }
      }

    DEBUG( dbgs() << "\n" );

  }

  //  static void GenerateInformationFlow();

};



// Vector of information flows.
typedef SmallVector<HardBlareInformationFlowBase*, 300> InformationFlows;

// Information flows for a particular Basic Block
typedef std::pair<MCSymbol*, InformationFlows*> BasicBlockInformationFlows;

// Vector of information flows.
typedef SmallVector<BasicBlockInformationFlows, 300> FunctionInformationFlows;

// Information flows for a Function
typedef std::map<unsigned, FunctionInformationFlows*> GlobalInformationFlows;




// Global InformationFlows Singleton
// ============================================

class GlobalInformationFlowsSingleton {
 private:
  //  static GlobalInformationFlowsSingleton* instance;
  GlobalInformationFlows* GIF;

  GlobalInformationFlowsSingleton();
  ~GlobalInformationFlowsSingleton();
  GlobalInformationFlowsSingleton(const GlobalInformationFlowsSingleton&) = delete;
  GlobalInformationFlowsSingleton& operator=(const GlobalInformationFlowsSingleton&) = delete;

 public:
  static GlobalInformationFlowsSingleton& getInstance();
  GlobalInformationFlows* getGlobalInformationFlows();
};




// Representation of a Monitor Information Flow
// ============================================
class HardBlareMonitorInformationFlow : public HardBlareInformationFlowBase {


 public:

  bool isStatic()
  {
    return false;
  }

  bool isDynamic()
  {
    return false;
  }

  bool isMonitor()
  {
    return true;
  }



  ~HardBlareMonitorInformationFlow()
    {
      DEBUG(errs() << "Destructor HardBlareMonitorInformationFlow" << "\n");
    }


  // Constructor
  HardBlareMonitorInformationFlow(MachineBasicBlock::instr_iterator MI)
    {

    }


  void dump()
  {

    for(auto Oit = Outs.begin(); Oit != Outs.end(); ++Oit)
      {
        (*Oit).dump();
        if(Oit != (--Outs.end()))
          {
            DEBUG( dbgs() << " | " );
          }
      }

    DEBUG( dbgs() << "  <===== ( Monitor, " << HardBlareInformationFlowHelpers::getARMOpTypeString(this->ARMOpType) << ", "  <<  HardBlareInformationFlowHelpers::getTagOpString(this->TagOp) << ")=====  " );

    for(auto Iit = Ins.begin(); Iit != Ins.end(); ++Iit)
      {
        (*Iit).dump();
        if(Iit != (--Ins.end()))
          {
            DEBUG( dbgs() << " | " );
          }
      }

    DEBUG( dbgs() << "\n" );

    //    emit();

  }


  void emit( AnnotMC& depVec )
  {

    DEBUG( dbgs() << " ----------------------------------------- \n" );
    DEBUG( dbgs() << "             EMIT DEPEDENCY MONITOR               \n" );
    DEBUG( dbgs() << " ----------------------------------------- \n" );

    if(ARMOpType == ARM_OPCODE_TYPE::UPDATE_TRACKED_VALUE_REG)
      {

        auto Dst = Outs.begin();
        auto Src = Ins.begin();
        unsigned Reg = 0;

        if( Dst->RegVal ==  ARM::SP)
          {
            Reg =  RegisterFiles::SP_VALUE;
          }
        else if( Dst->RegVal ==  ARM::LR)
          {
            Reg =  RegisterFiles::LR_VALUE;
          }
        else if( Dst->RegVal ==  ARM::R11)
          {
            Reg =  RegisterFiles::FP_VALUE;
          }

        auto annot = annot_Tag_rri(Reg,
                                   Reg,
                                   Src->ImmVal
                               );

        annot.dump();
        annot.emit(depVec);
      }

  }

};



// Representation of a Dynamic Information Flow
// ============================================
class HardBlareDynamicInformationFlow : public HardBlareInformationFlowBase {

 private:

  // Need instrumentation
  bool needInstr;


 public:


  // ARM Machine Instruction
  MachineInstr* MI;

  bool needInstrumentation()
  {
    return needInstr;
  }

  void setNeedInstrumentation(bool _v)
  {
    needInstr = _v;
  }

  bool isStatic()
  {
    return false;
  }

  bool isDynamic()
  {
    return true;
  }

  bool isMonitor()
  {
    return false;
  }


 HardBlareDynamicInformationFlow(MachineBasicBlock::instr_iterator _MI)
   : MI( &(*_MI) )
  {
    DEBUG(dbgs() << "\n HardBlareDynamicInformationFlow constructor MI = " << MI << " Pointer = : " <<  &(*_MI) << " \n");
    MI->dump();
  }


  static void GenerateHardBlareDynamicInformationFlow(MachineBasicBlock::instr_iterator MI, InformationFlows* _VecIF)
  {

    auto Desc = MI->getDesc();

    auto wb_mode = HardBlareOperand::NONE;

    auto isUpdatedValue = Desc.isWRITE_BACK();

    auto TagOp = TAG_OP::Init;

    auto needInstrumenation = true;

    // Partial Update
    if(Desc.modifyPartialyReg())
      {
        TagOp = TAG_OP::Update;
      }

    if(Desc.isPRE_INDEX())
      {
        wb_mode =  HardBlareOperand::PRE_INDEX;
      }
    else if(Desc.isPOST_INDEX())
      {
        wb_mode =  HardBlareOperand::POST_INDEX;
      }
    else if(Desc.isINCREMENT_AFTER())
      {
        wb_mode =  HardBlareOperand::INCREMENT_AFTER;
      }
    else if(Desc.isINCREMENT_BEFORE())
      {
        wb_mode =  HardBlareOperand::INCREMENT_BEFORE;
      }
    else if(Desc.isDECREMENT_AFTER())
      {
        wb_mode =  HardBlareOperand::DECREMENT_AFTER;
      }
    else if(Desc.isDECREMENT_BEFORE())
      {
        wb_mode =  HardBlareOperand::DECREMENT_BEFORE;
      }
    else
      {
        wb_mode = HardBlareOperand::NONE;
      }


    if(Desc.isREGISTER_BASE())
      {

        HardBlareInformationFlowBase* DynInfoFlow = new HardBlareDynamicInformationFlow(MI);

        MachineOperand &Rt = HardBlareInformationFlowHelpers::getRt(MI);
        MachineOperand &BaseAddr = HardBlareInformationFlowHelpers::getBaseAddr(MI);

        HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);
        HardBlareOperand BaseAddrHB = HardBlareOperand(HardBlareOperand::Address, wb_mode, BaseAddr);

        if( BaseAddr.getReg() == ARM::SP || (BaseAddr.getReg() == ARM::R11) || (BaseAddr.getReg() == ARM::PC) )
          {
            needInstrumenation = false;
          }

        DynInfoFlow->TagOp = TagOp;

        if(Desc.isSimpleLoad())
          {
            DynInfoFlow->Outs.insert(RtHB);
            DynInfoFlow->Ins.insert(BaseAddrHB);
            DynInfoFlow->setNeedInstrumentation(needInstrumenation);
            _VecIF->push_back(DynInfoFlow);
          }
        else if(Desc.isSimpleStore())
          {
            DynInfoFlow->Outs.insert(BaseAddrHB);
            DynInfoFlow->Ins.insert(RtHB);
            DynInfoFlow->setNeedInstrumentation(needInstrumenation);
            _VecIF->push_back(DynInfoFlow);
          }
        else
          {
            assert( false && "Error: NOT FOUND HARDBLAREINFORMATION DYNAMIC");
          }

        DynInfoFlow->dump();
      }

    else if(Desc.isREGISTER_BASE_OFFSET())
      {

        MachineOperand &Rt = HardBlareInformationFlowHelpers::getRt(MI);
        MachineOperand &BaseAddr = HardBlareInformationFlowHelpers::getBaseAddr(MI);
        MachineOperand &BaseOffset = HardBlareInformationFlowHelpers::getOffsetImmAddr(MI);

        // CFI_INDEX [pc, ±Offset]
        // We can here implement an Init tag to CONSTANT_POOL_INDEX in order to modify the security policy with the fact that we trust the linker or not.
        if(BaseAddr.isCPI())
          {
            DEBUG(errs() << "Addr is CPI" << "\n");

            HardBlareInformationFlowBase* DynInfoFlow = new HardBlareDynamicInformationFlow(MI);

            needInstrumenation = false;

            HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);
            HardBlareOperand TagHB = HardBlareOperand(HardBlareOperand::Tag, HardBlareOperand::CONSTANT_POOL_TAG);

            DynInfoFlow->Outs.insert(RtHB);
            DynInfoFlow->Ins.insert(TagHB);

            DynInfoFlow->TagOp =  TAG_OP::Init_Tag_Val;

            DynInfoFlow->setNeedInstrumentation(needInstrumenation);
            _VecIF->push_back(DynInfoFlow);

            return;
          }

        else
          {


            if( BaseAddr.getReg() == ARM::SP || (BaseAddr.getReg() == ARM::R11) || (BaseAddr.getReg() == ARM::PC) )
              {
                needInstrumenation = false;
              }

            HardBlareInformationFlowBase* DynInfoFlow = new HardBlareDynamicInformationFlow(MI);

            if(Desc.isSimpleLoad())
              {
                HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);
                HardBlareOperand BaseAddrHB = HardBlareOperand(HardBlareOperand::Address, wb_mode, BaseAddr, BaseOffset);

                DynInfoFlow->Outs.insert(RtHB);
                DynInfoFlow->Ins.insert(BaseAddrHB);
                DynInfoFlow->TagOp = TagOp;
                DynInfoFlow->setNeedInstrumentation(needInstrumenation);
                _VecIF->push_back(DynInfoFlow);
              }
            else if(Desc.isSimpleStore())
              {
                HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);
                HardBlareOperand BaseAddrHB = HardBlareOperand(HardBlareOperand::Address, wb_mode, BaseAddr, BaseOffset);

                DynInfoFlow->Outs.insert(BaseAddrHB);
                DynInfoFlow->Ins.insert(RtHB);
                DynInfoFlow->TagOp = TagOp;
                DynInfoFlow->setNeedInstrumentation(needInstrumenation);
                _VecIF->push_back(DynInfoFlow);
              }
            else
              {
                assert( false && "Error: NOT FOUND HARDBLAREINFORMATION DYNAMIC");
              }

            DynInfoFlow->dump();

          }
      }

    else if(Desc.isREGISTER_BASE_REGISTER_BASE())
      {

        MachineOperand &Rt = HardBlareInformationFlowHelpers::getRt(MI);
        MachineOperand &BaseAddr = HardBlareInformationFlowHelpers::getBaseAddr(MI);
        MachineOperand &Base2Addr = HardBlareInformationFlowHelpers::getOffsetRegAddr(MI);

        if( (BaseAddr.getReg() == ARM::SP || (BaseAddr.getReg() == ARM::R11) || (BaseAddr.getReg() == ARM::PC))
            &&  (Base2Addr.getReg() == ARM::SP || (Base2Addr.getReg() == ARM::R11) || (Base2Addr.getReg() == ARM::PC)) )
          {
            needInstrumenation = false;
          }

        HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);
        HardBlareOperand BaseAddrHB = HardBlareOperand(HardBlareOperand::Address, wb_mode, BaseAddr, Base2Addr);

        HardBlareInformationFlowBase* DynInfoFlow = new HardBlareDynamicInformationFlow(MI);

        if(Desc.isSimpleLoad())
          {
            DynInfoFlow->Outs.insert(RtHB);
            DynInfoFlow->Ins.insert(BaseAddrHB);
            DynInfoFlow->TagOp = TagOp;
            DynInfoFlow->setNeedInstrumentation(needInstrumenation);
            _VecIF->push_back(DynInfoFlow);
          }
        else if(Desc.isSimpleStore())
          {
            DynInfoFlow->Outs.insert(BaseAddrHB);
            DynInfoFlow->Ins.insert(RtHB);
            DynInfoFlow->TagOp = TagOp;
            DynInfoFlow->setNeedInstrumentation(needInstrumenation);
            _VecIF->push_back(DynInfoFlow);
          }
        else
          {
            assert( false && "Error: NOT FOUND HARDBLAREINFORMATION DYNAMIC");
          }

        DynInfoFlow->dump();

      }

    else if(Desc.isREGISTER_BASE_REGISTER_BASE_SHIFT())
      {

        MachineOperand &Rt = HardBlareInformationFlowHelpers::getRt(MI);
        MachineOperand &BaseAddr = HardBlareInformationFlowHelpers::getShiftBase(MI);
        MachineOperand &Base2Addr = HardBlareInformationFlowHelpers::getShiftBase2(MI);
        MachineOperand &ScalingAddr = HardBlareInformationFlowHelpers::getShiftImm(MI);

        if( (BaseAddr.getReg() == ARM::SP || (BaseAddr.getReg() == ARM::R11) || (BaseAddr.getReg() == ARM::PC))
            &&  (Base2Addr.getReg() == ARM::SP || (Base2Addr.getReg() == ARM::R11) || (Base2Addr.getReg() == ARM::PC)) )
          {
            needInstrumenation = false;
          }


        HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);
        HardBlareOperand BaseAddrHB = HardBlareOperand(HardBlareOperand::Address, wb_mode, BaseAddr, Base2Addr, ScalingAddr);

        HardBlareInformationFlowBase* DynInfoFlow = new HardBlareDynamicInformationFlow(MI);

        if(Desc.isSimpleLoad())
          {
            DynInfoFlow->Outs.insert(RtHB);
            DynInfoFlow->Ins.insert(BaseAddrHB);
            DynInfoFlow->TagOp = TagOp;
            DynInfoFlow->setNeedInstrumentation(needInstrumenation);
            _VecIF->push_back(DynInfoFlow);
          }
        else if(Desc.isSimpleStore())
          {
            DynInfoFlow->Outs.insert(BaseAddrHB);
            DynInfoFlow->Ins.insert(RtHB);
            DynInfoFlow->TagOp = TagOp;
            DynInfoFlow->setNeedInstrumentation(needInstrumenation);
            _VecIF->push_back(DynInfoFlow);
          }
        else
          {
            assert( false && "Error: NOT FOUND HARDBLAREINFORMATION DYNAMIC");
          }

        DynInfoFlow->dump();

      }

    else if( (Desc.isMultipleLoad()) || (Desc.isMultipleStore()) )
      {

        unsigned numberOfWords = 0;

        if( (Desc.isMultipleLoad()) )
          {

            HardBlareInformationFlowBase* DynInfoFlow = new HardBlareDynamicInformationFlow(MI);

            MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
            HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Address, wb_mode, Rn);
            DynInfoFlow->Ins.insert(RnHB);
            DynInfoFlow->TagOp = TagOp;

            if( Rn.getReg() == ARM::SP || (Rn.getReg() == ARM::R11) || (Rn.getReg() == ARM::PC) )
              {
                needInstrumenation = false;
              }

            DynInfoFlow->setNeedInstrumentation(needInstrumenation);

            int RegsOpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::regs);

            for(unsigned i = 0; i < MI->getNumOperands() - RegsOpIdx; i++)
              {
                MachineOperand &reg = HardBlareInformationFlowHelpers::getRegsI(MI, i);
                HardBlareOperand regHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, reg);
                DynInfoFlow->Outs.insert(regHB);
                numberOfWords++;
              }
            DynInfoFlow->dump();
            _VecIF->push_back(DynInfoFlow);

          }
        else
          {
            HardBlareInformationFlowBase* DynInfoFlow = new HardBlareDynamicInformationFlow(MI);

            MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
            HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Address, wb_mode, Rn);
            DynInfoFlow->Outs.insert(RnHB);

            DynInfoFlow->TagOp = TagOp;

            if( Rn.getReg() == ARM::SP || (Rn.getReg() == ARM::R11) || (Rn.getReg() == ARM::PC) )
              {
                needInstrumenation = false;
              }

            DynInfoFlow->setNeedInstrumentation(needInstrumenation);

            int RegsOpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::regs);
            for(unsigned i = 0; i < MI->getNumOperands() - RegsOpIdx; i++)
              {
                MachineOperand &reg = HardBlareInformationFlowHelpers::getRegsI(MI, i);
                HardBlareOperand regHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, reg);
                DynInfoFlow->Ins.insert(regHB);
                numberOfWords++;
              }
            DynInfoFlow->dump();
            _VecIF->push_back(DynInfoFlow);

          }



        if(isUpdatedValue)
          {

            HardBlareInformationFlowBase* MonitorTrackRegister = new HardBlareMonitorInformationFlow(MI);
            DEBUG(errs() << "Update Value of tracked register" << "\n");

            MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
            HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, HardBlareOperand::NONE, Rn);
            MonitorTrackRegister->Outs.insert(RnHB);

            int imm = 0;

            if( Desc.isDECREMENT_AFTER() || Desc.isDECREMENT_BEFORE() )
              {
                imm = numberOfWords * -4;
              }
            else if( Desc.isINCREMENT_AFTER() || Desc.isINCREMENT_BEFORE() )
              {
                imm = numberOfWords * 4;
              }

            HardBlareOperand ImmHB = HardBlareOperand(HardBlareOperand::Immediate, imm);
            MonitorTrackRegister->Ins.insert(ImmHB);

            MonitorTrackRegister->ARMOpType =  ARM_OPCODE_TYPE::UPDATE_TRACKED_VALUE_REG;
            MonitorTrackRegister->TagOp = TAG_OP::None;
            MonitorTrackRegister->dump();
            _VecIF->push_back(MonitorTrackRegister);

          }

      }

    else if( (Desc.isVFPLoad()) ||  (Desc.isVFPStore()) )
      {
        //        auto DynInfoFlow = HardBlareDynamicInformationFlow(_MI);
        //     _VecIF->push_back(DynInfoFlow);
      }

    else
      {
        DEBUG( dbgs() << "Error : HardBlareDynamicInformationFlow LDR/STR Opcode not found" );
      }

  }


  ~HardBlareDynamicInformationFlow()
    {
      DEBUG(errs() << "Destructor HardBlareDynamicInformationFlow" << "\n");
    }


  void dump()
  {

    for(auto Oit = Outs.begin(); Oit != Outs.end(); ++Oit)
      {
        (*Oit).dump();
        if(Oit != (--Outs.end()))
          {
            DEBUG( dbgs() << " | " );
          }
      }

    auto stringInstr =  (this->needInstrumentation()) ? "needInstrumentation" : "NOTneedInstrumentation" ;

    DEBUG( dbgs() << "  <===== ( Dynamic , " <<  HardBlareInformationFlowHelpers::getTagOpString(this->TagOp) << " , " << stringInstr <<  " )=====  " );

    for(auto Iit = Ins.begin(); Iit != Ins.end(); ++Iit)
      {
        (*Iit).dump();

        if(Iit != (--Ins.end()))
          {
            DEBUG( dbgs() << " | " );
          }
      }

    DEBUG( dbgs() << "\n" );

  }

  void emit( AnnotMC& depVec )
  {

    DEBUG( dbgs() << " ----------------------------------------- \n" );
    DEBUG( dbgs() << "             EMIT DEPEDENCY DYNAMI         \n" );
    DEBUG( dbgs() << " ----------------------------------------- \n" );


    auto ARMOpType = this->ARMOpType;

    // TAG_OP
    switch(TagOp)
      {

        // INIT FORMAT
      case(TAG_OP::Init):
        {

          auto Dst = Outs.begin();
          auto Src = Ins.begin();

          // Tag_mem_reg
          if(!isInsContainAddress() && isOutsContainAddress())
            {

              unsigned Reg = 0;

              int Offset = Dst->AddressVal.OffsetVal;

              if(  Dst->AddressVal.BaseRegVal ==  ARM::SP)
                {
                  Reg =  RegisterFiles::SP_VALUE;
                }
              else if( Dst->AddressVal.BaseRegVal ==  ARM::LR)
                {
                  Reg =  RegisterFiles::LR_VALUE;
                }
              else if( Dst->AddressVal.BaseRegVal ==  ARM::R11)
                {
                  Reg =  RegisterFiles::FP_VALUE;
                }


              auto annot = annot_Tag_mem_reg(Reg,
                                             Src->RegVal,
                                             Offset
                                             );
              annot.dump();
              annot.emit(depVec);
            }

        }
        break;

        // UPDATE FORMAT
      case(TAG_OP::Update):
        {
        }
        break;

        // CHECK FORMAT
      case(TAG_OP::Check):
        {
        }
        break;

      default:
        {
          break;
        }
      }
  }

};



// Representation of a Static Information Flow
// ============================================
class HardBlareStaticInformationFlow : public HardBlareInformationFlowBase {

 public:

  bool isStatic()
  {
    return true;
  }

  bool isDynamic()
  {
    return false;
  }

  bool isMonitor()
  {
    return false;
  }


  ~HardBlareStaticInformationFlow()
    {
      DEBUG(errs() << "Destructor HardBlareStaticInformationFlow" << "\n");
    }


  // Constructor
 HardBlareStaticInformationFlow(MachineBasicBlock::instr_iterator MI)
  {

    auto Desc = MI->getDesc();

    auto FlowsAppend = Desc.HardBlareFlowsAppend;

    auto wb_mode = HardBlareOperand::NONE;

    unsigned tmp = 0;

    ARMCC::CondCodes CC = llvm::getInstrPredicate(*MI, tmp);

    // ********************************************************************************
    // ********************************************************************************
    //                            Conditional Code
    //
    // -----------------
    //
    //    Add implicit information flows
    //
    // ********************************************************************************
    // ********************************************************************************

    DEBUG(errs() << "Conditional Code : " << CC << "\n");

    /*
      Code	                      |                  Meaning (for cmp or subs) 	         |         Flags Tested
      ------------------------------------------------------------------------------------------------------
      eq	                                          Equal.	                                Z==1
      ne                                          Not equal.                                Z==0
      cs or hs	                  Unsigned higher or same (or carry set).	                  C==1
      cc or lo	                        Unsigned lower (or carry clear).	                  C==0
      mi                      	Negative. The mnemonic stands for "minus".	                N==1
      pl	                    Positive or zero. The mnemonic stands for "plus".           	N==0
      vs	                     Signed overflow. The mnemonic stands for "V set".          	V==1
      vc	                   No signed overflow. The mnemonic stands for "V clear".	        V==0
      hi                                       	Unsigned higher.	                       (C==1) && (Z==0)
      ls                                     	Unsigned lower or same.                	   (C==0) || (Z==1)
      ge                                   	Signed greater than or equal.	                  N==V
      lt	                                           Signed less than.                      N!=V
      gt                                         	Signed greater than.	                 (Z==0) && (N==V)
      le                                    	Signed less than or equal.	               (Z==1) || (N!=V)
      al                                    (or omitted)	Always executed.	               None tested.
    */

    switch (CC) {

    case ARMCC::EQ:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.Z");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        Ins.insert(RegisterHB);
        break;
      }

    case ARMCC::NE:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.Z");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        Ins.insert(RegisterHB);
        break;
      }

    case ARMCC::HS:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.C");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        Ins.insert(RegisterHB);
        break;
      }

    case ARMCC::LO:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.C");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        Ins.insert(RegisterHB);
        break;
      }

    case ARMCC::MI:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.N");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        Ins.insert(RegisterHB);
        break;
      }

    case ARMCC::PL:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.N");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        Ins.insert(RegisterHB);
        break;
      }

    case ARMCC::VS:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.V");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        Ins.insert(RegisterHB);
        break;
      }

    case ARMCC::VC:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.V");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        Ins.insert(RegisterHB);
        break;
      }

    case ARMCC::HI:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.C");
        auto RegisterNumer2 = HardBlareOperand::getRegisterNumberFromString("CPSR.Z");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        HardBlareOperand RegisterHB2 = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer2);
        Ins.insert(RegisterHB);
        Ins.insert(RegisterHB2);
        break;
      }

    case ARMCC::LS:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.C");
        auto RegisterNumer2 = HardBlareOperand::getRegisterNumberFromString("CPSR.Z");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        HardBlareOperand RegisterHB2 = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer2);
        Ins.insert(RegisterHB);
        Ins.insert(RegisterHB2);
        break;
      }

    case ARMCC::GE:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.N");
        auto RegisterNumer2 = HardBlareOperand::getRegisterNumberFromString("CPSR.V");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        HardBlareOperand RegisterHB2 = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer2);
        Ins.insert(RegisterHB);
        Ins.insert(RegisterHB2);
        break;
      }

    case ARMCC::LT:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.N");
        auto RegisterNumer2 = HardBlareOperand::getRegisterNumberFromString("CPSR.V");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        HardBlareOperand RegisterHB2 = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer2);
        Ins.insert(RegisterHB);
        Ins.insert(RegisterHB2);
        break;
      }

    case ARMCC::GT:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.N");
        auto RegisterNumer2 = HardBlareOperand::getRegisterNumberFromString("CPSR.V");
        auto RegisterNumer3 = HardBlareOperand::getRegisterNumberFromString("CPSR.Z");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        HardBlareOperand RegisterHB2 = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer2);
        HardBlareOperand RegisterHB3 = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer3);
        Ins.insert(RegisterHB);
        Ins.insert(RegisterHB2);
        Ins.insert(RegisterHB3);
        break;
      }

    case ARMCC::LE:
      {
        auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString("CPSR.N");
        auto RegisterNumer2 = HardBlareOperand::getRegisterNumberFromString("CPSR.V");
        auto RegisterNumer3 = HardBlareOperand::getRegisterNumberFromString("CPSR.Z");
        HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
        HardBlareOperand RegisterHB2 = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer2);
        HardBlareOperand RegisterHB3 = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer3);
        Ins.insert(RegisterHB);
        Ins.insert(RegisterHB2);
        Ins.insert(RegisterHB3);
        break;
      }

    case ARMCC::AL:
      // Unconditional Nothing to do
      break;
    }


    if(FlowsAppend.empty())
      {
        DEBUG(errs() << "HardBlareFlowsAppend is empty\n");
      }

    else if( (!FlowsAppend.empty()) && (FlowsAppend.front().size() == 0) )
      {
        DEBUG(errs() << "HardBlareFlowsAppend is empty\n");
      }

    // Check if there is a 2*n number of vector element
    // Otherwise the Flows are malformed
    else if((FlowsAppend.size() % 2) != 0)
      {
        DEBUG(errs() << "HardBlareFlowsAppend : MALFORMED \n");
      }

    else
      {
        for(auto itFlow = FlowsAppend.begin(); itFlow != FlowsAppend.end(); itFlow+=2)
          {
            // Outs
            auto OutsFlow = *(itFlow);
            // Inputs
            auto InsFlow = *(itFlow + 1);
            // Flow
            for(auto OutR : OutsFlow)
              {
                if(OutR == "")
                  {
                  }
                else
                  {
                    DEBUG(errs() << "HardBlareFlowsAppend OutR : " << OutR << "\n");
                    auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString(OutR);
                    HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
                    Outs.insert(RegisterHB);
                  }
              }

            for(auto InR : InsFlow)
              {
                if(InR == "")
                  {
                  }
                else
                  {
                    DEBUG(errs() << "HardBlareFlowsAppend InR : " << InR << "\n");
                    auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString(InR);
                    HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
                    Ins.insert(RegisterHB);
                  }
              }
          }
      }
  }


// Constructor
  static void GenerateHardBlareStaticInformationFlow(MachineBasicBlock::instr_iterator MI, InformationFlows* _VecIF)
  {

    auto Desc = MI->getDesc();
    auto wb_mode = HardBlareOperand::NONE;

    auto ARMOpType = HardBlareInformationFlowHelpers::getARMOpType(MI);

    auto TagOp = TAG_OP::Init;

    auto MBB = MI->getParent();
    auto MF = MBB->getParent();
    const TargetRegisterInfo *TRI = MF->getSubtarget().getRegisterInfo();

    // Partial Update
    if(Desc.modifyPartialyReg())
      {
        TagOp = TAG_OP::Update;
      }

    // We track all Updates of SP, FP (R11)
    if( (MI->modifiesRegister(ARM::SP, TRI)) ||  (MI->modifiesRegister(ARM::R11, TRI)) )
      {

        if(Desc.isMoveOp())
          {

            if(Desc.isRdRm())
              {
                // Flow
                HardBlareInformationFlowBase* MonitorTrackRegister = new HardBlareMonitorInformationFlow(MI);
                DEBUG(errs() << "Move Instruction: with SP or FP" << "\n");
                MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
                MachineOperand &Rm = HardBlareInformationFlowHelpers::getRm(MI);
                HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
                HardBlareOperand RmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rm);
                MonitorTrackRegister->Outs.insert(RdHB);
                MonitorTrackRegister->Ins.insert(RmHB);
                MonitorTrackRegister->ARMOpType =  ARM_OPCODE_TYPE::MOVE_VALUE_TRACKED_REG;
                MonitorTrackRegister->TagOp = TagOp;
                MonitorTrackRegister->dump();
                _VecIF->push_back(MonitorTrackRegister);

                // End
                return;
              }
          }

        else if(Desc.isArithLogicOp())
          {
            // Flow
            HardBlareInformationFlowBase* MonitorTrackRegister = new HardBlareStaticInformationFlow(MI);

            if (MI->modifiesRegister(ARM::SP, TRI))
              {
                DEBUG(errs() << "Instruction Modify SP " << "\n");

                MonitorTrackRegister->ARMOpType = ARM_OPCODE_TYPE::UPDATE_TRACKED_VALUE_REG;
                int32_t immediate_value = 0;

                MachineOperand &Imm = HardBlareInformationFlowHelpers::getImm(MI);

                if(Desc.isAdd())
                  {
                    immediate_value = Imm.getImm();
                  }
                else
                  {
                    immediate_value = -(Imm.getImm());
                  }

                HardBlareOperand ImmHB = HardBlareOperand(HardBlareOperand::Immediate, immediate_value);

                HardBlareOperand SpHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, HardBlareOperand::SP);

                MonitorTrackRegister->Outs.insert(SpHB);
                MonitorTrackRegister->Ins.insert(ImmHB);
                MonitorTrackRegister->TagOp = TagOp;

                MonitorTrackRegister->dump();
                _VecIF->push_back(MonitorTrackRegister);

                // End
                return;

              }
            else
              {
                DEBUG(errs() << "Instruction Modify FP " << "\n");
                //        MonitorInfoFlow->ARMOpType = ARM_OPCODE_TYPE::UPDATE_FP;
              }
          }

        else
          {
            DEBUG(errs() << "Modify FP or SP but solution NOT FOUND " << "\n");
            //        MonitorInfoFlow->ARMOpType = ARM_OPCODE_TYPE::UPDATE_FP;
          }

      }

    auto FlowsBefore = Desc.HardBlareFlowsBefore;
    auto FlowsAfter = Desc.HardBlareFlowsAfter;

    // Check if FlowsBefore is empty
    if(FlowsBefore.empty())
      {
        DEBUG(errs() << "HardBlareFlowsBefore is empty\n");
      }
    // Check if FlowsBefore is empty
    if( (!FlowsBefore.empty()) && (FlowsBefore.front().size() == 0) )
      {
        DEBUG(errs() << "HardBlareFlowsBefore is empty\n");
      }

    // Check if there is a 2*n number of vector element
    // Otherwise the Flows are malformed
    else if((FlowsBefore.size() % 2) != 0)
      {
          DEBUG(errs() << "HardBlareFlowsBefore : MALFORMED \n");
      }
    else
      {
        for(auto itFlow = FlowsBefore.begin(); itFlow != FlowsBefore.end(); itFlow+=2)
          {
            // Outs
            auto OutsFlow = *(itFlow);
            // Inputs
            auto InsFlow = *(itFlow + 1);
            // Flow
            HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

            for(auto OutR : OutsFlow)
              {
                if(OutR == "")
                  {
                  }
                else
                  {
                    DEBUG(errs() << "HardBlareFlowsBefore OutR : " << OutR << "\n");
                    auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString(OutR);
                    HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
                    StaticInfoFlow->Outs.insert(RegisterHB);
                  }
              }

            for(auto InR : InsFlow)
              {
                if(InR == "")
                  {
                  }
                else
                  {
                    DEBUG(errs() << "HardBlareFlowsBefore InR : " << InR << "\n");
                    auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString(InR);
                    HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
                    StaticInfoFlow->Ins.insert(RegisterHB);
                  }
              }

            StaticInfoFlow->ARMOpType = ARMOpType;
            StaticInfoFlow->TagOp = TagOp;
            StaticInfoFlow->dump();
            _VecIF->push_back(StaticInfoFlow);

          }
      }

    if(MI->isCall() || MI->isBranch())
      {

        ARMOpType =  ARM_OPCODE_TYPE::CALL_RET;

        if(MI->isConditionalBranch())
          {
            HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
            HardBlareOperand pcHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, HardBlareOperand::PC);
            StaticInfoFlow->Outs.insert(pcHB);
            StaticInfoFlow->ARMOpType = ARMOpType;
            StaticInfoFlow->TagOp = TagOp;
            StaticInfoFlow->dump();
            _VecIF->push_back(StaticInfoFlow);
          }
        else
          {
            // Nothing to do
          }

        /*        if(MI->getOpcode() == ARM::BL)
            {

              HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
            DEBUG(errs() << "HardBlareStaticInformationFlow: is BL" << "\n");
            HardBlareOperand pcHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, HardBlareOperand::PC);
            HardBlareOperand lrHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, HardBlareOperand::LR);
            StaticInfoFlow->Ins.insert(pcHB);
            StaticInfoFlow->Outs.insert(lrHB);
            StaticInfoFlow->ARMOpType = ARMOpType;
            StaticInfoFlow->TagOp = TagOp;
            StaticInfoFlow->dump();
            _VecIF->push_back(StaticInfoFlow);
          }
        else
          {
            // Nothing to do
          }
        */
      }

    /*
    if(Desc.isReturn())
      {

        if(Desc.isMoveOp())
          {
            HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

            DEBUG(errs() << "HardBlareStaticInformationFlow: is MOVEPCLR" << "\n");
            HardBlareOperand pcHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, HardBlareOperand::PC);
            HardBlareOperand lrHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, HardBlareOperand::LR);
            StaticInfoFlow->Outs.insert(pcHB);
            StaticInfoFlow->Ins.insert(lrHB);
            StaticInfoFlow->ARMOpType = ARMOpType;
            StaticInfoFlow->TagOp = TagOp;
            StaticInfoFlow->dump();
            _VecIF->push_back(StaticInfoFlow);
          }

      }
    else
    */

      if(Desc.isRd())
      {

        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRd" << "\n");
        MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
        HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
        StaticInfoFlow->Outs.insert(RdHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRdShift())
        {
          HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
          DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdShift" << "\n");
          MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
          HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
          //          HardBlareOperand RdHB = HardBlareOperand(Rd);
          StaticInfoFlow->Outs.insert(RdHB);
          StaticInfoFlow->ARMOpType = ARMOpType;
          StaticInfoFlow->TagOp = TagOp;
          StaticInfoFlow->dump();
          _VecIF->push_back(StaticInfoFlow);
        }
    else if(Desc.isRdImm())
      {
        // Nothing to do
      }
    else if(Desc.isRdImmShift())
      {
        // Nothing to do
        if(Desc.isMoveOp())
          {
          }
        else
          {
            HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
            DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdImmShift" << "\n");
            MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
            HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
            //        HardBlareOperand RdHB = HardBlareOperand(Rd);
            StaticInfoFlow->Outs.insert(RdHB);
            StaticInfoFlow->ARMOpType = ARMOpType;
            StaticInfoFlow->TagOp = TagOp;
            StaticInfoFlow->dump();
            _VecIF->push_back(StaticInfoFlow);
          }
      }
    else if(Desc.isRdRn())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdRn" << "\n");
        MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
        MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
        HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
        //        HardBlareOperand RdHB = HardBlareOperand(Rd);
        //HardBlareOperand RnHB = HardBlareOperand(Rn);
        HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);
        StaticInfoFlow->Outs.insert(RdHB);
        StaticInfoFlow->Ins.insert(RnHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRdRnShift())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdRnShift" << "\n");
        MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
        MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
        //HardBlareOperand RdHB = HardBlareOperand(Rd);
        //        HardBlareOperand RnHB = HardBlareOperand(Rn);
        HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
        HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);
        StaticInfoFlow->Outs.insert(RdHB);
        StaticInfoFlow->Ins.insert(RnHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRdRnImm())
      {
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdRnImm" << "\n");
        MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
        MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);

        /*    if(HardBlareInformationFlowHelpers::isUpdateSP( MI, Rd ))
          {

            HardBlareInformationFlowBase* MonitorInfoFlow = new HardBlareStaticInformationFlow(MI);

            HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);

            MachineOperand &Imm = HardBlareInformationFlowHelpers::getImm(MI);
            HardBlareOperand ImmHB = HardBlareOperand(HardBlareOperand::Immediate, wb_mode, Imm);

            MonitorInfoFlow->ARMOpType = ARM_OPCODE_TYPE::UPDATE_SP;
            MonitorInfoFlow->TagOp = TagOp;

            MonitorInfoFlow->Outs.insert(RdHB);
            MonitorInfoFlow->Ins.insert(ImmHB);

            MonitorInfoFlow->dump();

            _VecIF->push_back(MonitorInfoFlow);
          }
        else
          {
        */
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

            HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
            HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);

            StaticInfoFlow->ARMOpType = ARMOpType;
            StaticInfoFlow->TagOp = TagOp;

            StaticInfoFlow->Outs.insert(RdHB);
            StaticInfoFlow->Ins.insert(RnHB);

            StaticInfoFlow->dump();
            _VecIF->push_back(StaticInfoFlow);
            // }

      }
    else if(Desc.isRdRnImmShift())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdRnImmShift" << "\n");
        MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
        MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
        //HardBlareOperand RdHB = HardBlareOperand(Rd);
        //HardBlareOperand RnHB = HardBlareOperand(Rn);

        HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
        HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);

        StaticInfoFlow->Outs.insert(RdHB);
        StaticInfoFlow->Ins.insert(RnHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRdRnRm())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdRnRm" << "\n");
        MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
        MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
        MachineOperand &Rm = HardBlareInformationFlowHelpers::getRm(MI);
        //        HardBlareOperand RdHB = HardBlareOperand(Rd);
        // HardBlareOperand RnHB = HardBlareOperand(Rn);
        // HardBlareOperand RmHB = HardBlareOperand(Rm);

        HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
        HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);
        HardBlareOperand RmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rm);

        StaticInfoFlow->Outs.insert(RdHB);
        StaticInfoFlow->Ins.insert(RnHB);
        StaticInfoFlow->Ins.insert(RmHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRdRm())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdRm" << "\n");
        MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
        MachineOperand &Rm = HardBlareInformationFlowHelpers::getRm(MI);
        HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
        HardBlareOperand RmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rm);
        StaticInfoFlow->Outs.insert(RdHB);
        StaticInfoFlow->Ins.insert(RmHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRdRmShift())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdRmShift" << "\n");
        MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
        MachineOperand &Rm = HardBlareInformationFlowHelpers::getRm(MI);
        HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
        HardBlareOperand RmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rm);
        StaticInfoFlow->Outs.insert(RdHB);
        StaticInfoFlow->Ins.insert(RmHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRdRmRn())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdRmRn" << "\n");
        MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
        MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
        MachineOperand &Rm = HardBlareInformationFlowHelpers::getRm(MI);
        HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
        HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);
        HardBlareOperand RmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rm);
        StaticInfoFlow->Outs.insert(RdHB);
        StaticInfoFlow->Ins.insert(RnHB);
        StaticInfoFlow->Ins.insert(RmHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRdRmRnRa())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdRmRnRa" << "\n");
        MachineOperand &Rd = HardBlareInformationFlowHelpers::getRd(MI);
        MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
        MachineOperand &Rm = HardBlareInformationFlowHelpers::getRm(MI);
        MachineOperand &Ra = HardBlareInformationFlowHelpers::getRa(MI);
        HardBlareOperand RdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rd);
        HardBlareOperand RnHB = HardBlareOperand( HardBlareOperand::Register, wb_mode, Rn);
        HardBlareOperand RmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rm);
        HardBlareOperand RaHB =  HardBlareOperand(HardBlareOperand::Register, wb_mode, Ra);
        StaticInfoFlow->Outs.insert(RdHB);
        StaticInfoFlow->Ins.insert(RnHB);
        StaticInfoFlow->Ins.insert(RmHB);
        StaticInfoFlow->Ins.insert(RaHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }


    // isRdHiRdLoRnRm
    else if(Desc.isRdHiRdLoRnRm())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRdRmRnRa" << "\n");
        MachineOperand &RdHi = HardBlareInformationFlowHelpers::getRdHi(MI);
        MachineOperand &RdLo = HardBlareInformationFlowHelpers::getRdLo(MI);
        MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
        MachineOperand &Rm = HardBlareInformationFlowHelpers::getRm(MI);
        HardBlareOperand RdHiHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RdHi);
        HardBlareOperand RdLoHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RdLo);
        HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);
        HardBlareOperand RmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rm);
        StaticInfoFlow->Outs.insert(RdHiHB);
        StaticInfoFlow->Ins.insert(RdLoHB);
        StaticInfoFlow->Ins.insert(RnHB);
        StaticInfoFlow->Ins.insert(RmHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }

    // is Regs Rn
    else if(Desc.isRegsRn())
      {
        // HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRegsRn TODO " << "\n");
      }
    // isRegsRn
    else if(Desc.isRnShift())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRnShift TODO " << "\n");
        MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
        HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);
        StaticInfoFlow->Ins.insert(RnHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRnImm())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRnImm TODO " << "\n");
        MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
        HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);
        StaticInfoFlow->Ins.insert(RnHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRnImmShift())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRnImmShift TODO " << "\n");
        MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
        HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);
        StaticInfoFlow->Ins.insert(RnHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRnRm())
      {

        if(Desc.isCompOp())
          {
            HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
            DEBUG(errs() << "HardBlareStaticInformationFlow: is isRnRm" << "\n");
            MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
            MachineOperand &Rm = HardBlareInformationFlowHelpers::getRm(MI);
            HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);
            HardBlareOperand RmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rm);
            StaticInfoFlow->Ins.insert(RnHB);
            StaticInfoFlow->Ins.insert(RmHB);
            StaticInfoFlow->ARMOpType = ARMOpType;
            StaticInfoFlow->TagOp = TagOp;
            StaticInfoFlow->dump();
            _VecIF->push_back(StaticInfoFlow);
          }
        else
          {
            HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
            DEBUG(errs() << "HardBlareStaticInformationFlow: is isRnRm" << "\n");
            MachineOperand &Rn = HardBlareInformationFlowHelpers::getRn(MI);
            MachineOperand &Rm = HardBlareInformationFlowHelpers::getRm(MI);
            HardBlareOperand RnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rn);
            HardBlareOperand RmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rm);
            StaticInfoFlow->Outs.insert(RnHB);
            StaticInfoFlow->Ins.insert(RmHB);
            StaticInfoFlow->ARMOpType = ARMOpType;
            StaticInfoFlow->TagOp = TagOp;
            StaticInfoFlow->dump();
            _VecIF->push_back(StaticInfoFlow);
          }
      }


    // isRt..

    else if(Desc.isRtShift())
      {
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRtShift TODO " << "\n");
        MachineOperand &Rt = HardBlareInformationFlowHelpers::getRt(MI);
        HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);
        StaticInfoFlow->Outs.insert(RtHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isRtAddr())
      {

        MachineOperand &Addr = HardBlareInformationFlowHelpers::getBaseAddr(MI);

        if(Addr.isCPI())
          {

            HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

            MachineOperand &Rt = HardBlareInformationFlowHelpers::getRt(MI);
            HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);
            HardBlareOperand TagHB = HardBlareOperand(HardBlareOperand::Tag, HardBlareOperand::CONSTANT_POOL_TAG);

            StaticInfoFlow->Outs.insert(RtHB);
            StaticInfoFlow->Ins.insert(TagHB);

            StaticInfoFlow->ARMOpType = ARMOpType;
            StaticInfoFlow->TagOp = TagOp;
            StaticInfoFlow->dump();

            _VecIF->push_back(StaticInfoFlow);
          }
        else
          {
            HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
            HardBlareOperand AddrHB = HardBlareOperand(HardBlareOperand::Address, wb_mode, Addr);

            DEBUG(errs() << "HardBlareStaticInformationFlow: is isRtAddr " << "\n");
            MachineOperand &Rt = HardBlareInformationFlowHelpers::getRt(MI);
            HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);

            StaticInfoFlow->Ins.insert(RtHB);
            StaticInfoFlow->Outs.insert(AddrHB);
            StaticInfoFlow->ARMOpType = ARMOpType;
            StaticInfoFlow->TagOp = TagOp;
            StaticInfoFlow->dump();
            _VecIF->push_back(StaticInfoFlow);
          }
      }
    else if(Desc.isRtRt2Addr())
      {
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isRtRt2Addr TODO " << "\n");
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

        MachineOperand &Addr = HardBlareInformationFlowHelpers::getBaseAddr(MI);
        HardBlareOperand AddrHB = HardBlareOperand(HardBlareOperand::Address, wb_mode, Addr);

        MachineOperand &Rt = HardBlareInformationFlowHelpers::getRt(MI);
        HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);

        MachineOperand &Rt2 = HardBlareInformationFlowHelpers::getRt2(MI);
        HardBlareOperand Rt2HB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt2);

        StaticInfoFlow->Ins.insert(RtHB);
        StaticInfoFlow->Ins.insert(Rt2HB);
        StaticInfoFlow->Outs.insert(AddrHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isDdAddr())
      {
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isDdAddr TODO " << "\n");

        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        MachineOperand &Addr = HardBlareInformationFlowHelpers::getBaseAddr(MI);
        HardBlareOperand AddrHB = HardBlareOperand(HardBlareOperand::Address, wb_mode, Addr);

        MachineOperand &Dd = HardBlareInformationFlowHelpers::getDd(MI);
        HardBlareOperand DdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Dd);

        StaticInfoFlow->Ins.insert(DdHB);
        StaticInfoFlow->Outs.insert(AddrHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);

      }
    else if(Desc.isDdSm())
      {
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isDdSm TODO " << "\n");
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

        MachineOperand &Sm = HardBlareInformationFlowHelpers::getSm(MI);
        HardBlareOperand SmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Sm);

        MachineOperand &Dd = HardBlareInformationFlowHelpers::getDd(MI);
        HardBlareOperand DdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Dd);

        StaticInfoFlow->Ins.insert(SmHB);
        StaticInfoFlow->Outs.insert(DdHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);

      }
    else if(Desc.isDdDm())
      {
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isDdSm TODO " << "\n");
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

        MachineOperand &Dm = HardBlareInformationFlowHelpers::getDm(MI);
        HardBlareOperand DmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Dm);

        MachineOperand &Dd = HardBlareInformationFlowHelpers::getDd(MI);
        HardBlareOperand DdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Dd);

        StaticInfoFlow->Ins.insert(DmHB);
        StaticInfoFlow->Outs.insert(DdHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);

      }
    else if(Desc.isDdDnDm())
      {
        // HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isDdDnDm TODO " << "\n");
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

        MachineOperand &Dm = HardBlareInformationFlowHelpers::getDm(MI);
        HardBlareOperand DmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Dm);

        MachineOperand &Dn = HardBlareInformationFlowHelpers::getDn(MI);
        HardBlareOperand DnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Dn);

        MachineOperand &Dd = HardBlareInformationFlowHelpers::getDd(MI);
        HardBlareOperand DdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Dd);

        StaticInfoFlow->Ins.insert(DmHB);
        StaticInfoFlow->Ins.insert(DnHB);

        StaticInfoFlow->Outs.insert(DdHB);
        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);

      }
    else if(Desc.isDmRtRt2())
      {
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isDmRtRt2 TODO " << "\n");
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

        MachineOperand &Dm = HardBlareInformationFlowHelpers::getDm(MI);
        HardBlareOperand DmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Dm);

        MachineOperand &Rt = HardBlareInformationFlowHelpers::getRt(MI);
        HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);

        MachineOperand &Rt2 = HardBlareInformationFlowHelpers::getRt2(MI);
        HardBlareOperand Rt2HB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt2);

        StaticInfoFlow->Outs.insert(DmHB);
        StaticInfoFlow->Ins.insert(RtHB);
        StaticInfoFlow->Ins.insert(Rt2HB);

        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }
    else if(Desc.isSnRt())
      {

        DEBUG(errs() << "HardBlareStaticInformationFlow: is isSnRt TODO " << "\n");
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

        MachineOperand &Sn = HardBlareInformationFlowHelpers::getSn(MI);
        HardBlareOperand SnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Sn);

        MachineOperand &Rt = HardBlareInformationFlowHelpers::getRt(MI);
        HardBlareOperand RtHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Rt);

        StaticInfoFlow->Ins.insert(SnHB);
        StaticInfoFlow->Outs.insert(RtHB);

        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);

      }
    else if(Desc.isSdAddr())
      {
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isSdAddr TODO " << "\n");

        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

        MachineOperand &Sd = HardBlareInformationFlowHelpers::getSd(MI);
        HardBlareOperand SdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Sd);

        MachineOperand &Addr = HardBlareInformationFlowHelpers::getBaseAddr(MI);
        HardBlareOperand AddrHB = HardBlareOperand(HardBlareOperand::Address, wb_mode, Addr);

        StaticInfoFlow->Ins.insert(AddrHB);
        StaticInfoFlow->Outs.insert(SdHB);

        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);

      }
    else if(Desc.isSdDm())
      {
         DEBUG(errs() << "HardBlareStaticInformationFlow: is isSdDm TODO " << "\n");

         HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

         MachineOperand &Sd = HardBlareInformationFlowHelpers::getSd(MI);
         HardBlareOperand SdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Sd);

         MachineOperand &Dm = HardBlareInformationFlowHelpers::getDm(MI);
         HardBlareOperand DmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Dm);

         StaticInfoFlow->Ins.insert(DmHB);
         StaticInfoFlow->Outs.insert(SdHB);

         StaticInfoFlow->ARMOpType = ARMOpType;
         StaticInfoFlow->TagOp = TagOp;
         StaticInfoFlow->dump();
         _VecIF->push_back(StaticInfoFlow);

      }
    else if(Desc.isSdSm())
      {
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isSdSm TODO " << "\n");
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

        MachineOperand &Sd = HardBlareInformationFlowHelpers::getSd(MI);
        HardBlareOperand SdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Sd);

        MachineOperand &Sm = HardBlareInformationFlowHelpers::getSm(MI);
        HardBlareOperand SmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Sm);

        StaticInfoFlow->Ins.insert(SmHB);
        StaticInfoFlow->Outs.insert(SdHB);

        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);

      }
    else if(Desc.isSdSnSm())
      {
        DEBUG(errs() << "HardBlareStaticInformationFlow: is isSdSnSm TODO " << "\n");
        HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

        MachineOperand &Sd = HardBlareInformationFlowHelpers::getSd(MI);
        HardBlareOperand SdHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Sd);

        MachineOperand &Sn = HardBlareInformationFlowHelpers::getSn(MI);
        HardBlareOperand SnHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Sn);

        MachineOperand &Sm = HardBlareInformationFlowHelpers::getSm(MI);
        HardBlareOperand SmHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, Sm);

        StaticInfoFlow->Ins.insert(SnHB);
        StaticInfoFlow->Ins.insert(SmHB);
        StaticInfoFlow->Outs.insert(SdHB);

        StaticInfoFlow->ARMOpType = ARMOpType;
        StaticInfoFlow->TagOp = TagOp;
        StaticInfoFlow->dump();
        _VecIF->push_back(StaticInfoFlow);
      }

    else
      {
        //        assert(true && "HardBlareStaticInformationFlow : Error Format Not Found");
      }


    if(FlowsAfter.empty())
      {
        DEBUG(errs() << "HardBlareFlowsAfter is empty\n");
      }
    else if( (!FlowsAfter.empty()) && (FlowsAfter.front().size() == 0) )
      {
        DEBUG(errs() << "HardBlareFlowsAfter is empty\n");
      }
    // Check if there is a 2*n number of vector element
    // Otherwise the Flows are malformed
    else if((FlowsAfter.size() % 2) != 0)
      {
        DEBUG(errs() << "HardBlareFlowsAfter : MALFORMED \n");
      }

    else
      {
        for(auto itFlow = FlowsAfter.begin(); itFlow != FlowsAfter.end(); itFlow+=2)
          {
            // Outs
            auto OutsFlow = *(itFlow);
            // Inputs
            auto InsFlow = *(itFlow + 1);
            // Flow
            HardBlareInformationFlowBase* StaticInfoFlow = new HardBlareStaticInformationFlow(MI);

            for(auto OutR : OutsFlow)
              {
                if(OutR == "")
                  {
                  }
                else
                  {
                    DEBUG(errs() << "HardBlareFlowsAfter OutR : " << OutR << "\n");
                    auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString(OutR);
                    HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
                    StaticInfoFlow->Outs.insert(RegisterHB);
                  }
              }

            for(auto InR : InsFlow)
              {
                if(InR == "")
                  {
                  }
                else
                  {
                    DEBUG(errs() << "HardBlareFlowsAfter InR : " << InR << "\n");
                    auto RegisterNumer = HardBlareOperand::getRegisterNumberFromString(InR);
                    HardBlareOperand RegisterHB = HardBlareOperand(HardBlareOperand::Register, wb_mode, RegisterNumer);
                    StaticInfoFlow->Ins.insert(RegisterHB);
                  }
              }

            StaticInfoFlow->ARMOpType = ARMOpType;
            StaticInfoFlow->TagOp = TagOp;
            StaticInfoFlow->dump();
            _VecIF->push_back(StaticInfoFlow);

          }
      }

  }


  void dump()
  {

    for(auto Oit = Outs.begin(); Oit != Outs.end(); ++Oit)
      {
        (*Oit).dump();
        if(Oit != (--Outs.end()))
          {
            DEBUG( dbgs() << " | " );
          }
      }

    DEBUG( dbgs() << "  <===== ( Static, " << HardBlareInformationFlowHelpers::getARMOpTypeString(this->ARMOpType) << ", "  <<  HardBlareInformationFlowHelpers::getTagOpString(this->TagOp) << ")=====  " );

    for(auto Iit = Ins.begin(); Iit != Ins.end(); ++Iit)
      {
        (*Iit).dump();
        if(Iit != (--Ins.end()))
          {
            DEBUG( dbgs() << " | " );
          }
      }

    DEBUG( dbgs() << "\n" );

    //    emit();

  }


  void emit( AnnotMC& depVec )
  {

    DEBUG( dbgs() << " ----------------------------------------- \n" );
    DEBUG( dbgs() << "             EMIT DEPEDENCY STATIC               \n" );
    DEBUG( dbgs() << " ----------------------------------------- \n" );

    auto ARMOpType = this->ARMOpType;

    // TAG_OP
    switch(TagOp)
      {

      // INIT FORMAT
      case(TAG_OP::Init):
        {

          if(ARMOpType == ARM_OPCODE_TYPE::MOVE_VALUE_TRACKED_REG)
            {
              
            }

          // Tag_mem_tr_2
          if( isInsContainAddress() && !isOutsContainAddress())
            {
            }

          // Tag_tr_mem_2
          if( !isInsContainAddress() && isOutsContainAddress())
            {
            }

          // Tag_instrumentation_tr

          // Tag_tr_instrumentation

          // Tag_Kblare_tr

          // Tag_tr_Kblare

        }
        break;

        // UPDATE FORMAT
      case(TAG_OP::Update):
        {

          auto _ARMOpType = ARMOpType;
          auto SizeSrc = Ins.size();


          // TRR (TR format)
          // Ex. : r0, r1 <= r3, r4, r5
          if(isOnlyReg()){
            for( auto Dst = Outs.begin(); (Dst != Outs.end()); Dst++)
              {
                for( auto Src = Ins.begin(); (Src != Ins.end()) && (++Src != Ins.end()); (Src++)++)
                  {
                    if( (++Src == Ins.end()) )
                      {
                        auto annot = annot_TRR(_ARMOpType,
                                               Dst->RegVal,
                                               Dst->RegVal,
                                               (++Src)->RegVal,
                                               0
                                               );

                        annot.dump();
                        annot.emit(depVec);
                      }
                    else
                      {
                        auto annot = annot_TRR(_ARMOpType,
                                               Dst->RegVal,
                                               Src->RegVal,
                                               (++Src)->RegVal,
                                               0
                                               );
                        annot.dump();
                        annot.emit(depVec);
                      }
                  }
              }
          }


          // Tag_mem_tr
          if( isInsContainAddress() && !isOutsContainAddress())
            {
            }

          // Tag_tr_mem
          if( !isInsContainAddress() && isOutsContainAddress())
            {
            }

          // Tag_arith_log
          //          if()
          // {
          // }

        }
        break;


        // CHECK FORMAT
      case(TAG_OP::Check):
        {

          /*
          // Tag_check_reg

          // Tag_check_mem

          */
        }
        break;

      default:
        {
          break;
        }
      }
  }
};



  // Information Flows Helper Function
  // ============================================
class HardBlareInformationFlow {

 public:
    // ============================================
    // Generate Information Flow
    // ============================================
  static void GenerateInformationFlows(MachineFunction &MF,  GlobalInformationFlows& _GlobalInformationFlows);

  static void dump(GlobalInformationFlows& _GlobalInformationFlows);

};

#endif
