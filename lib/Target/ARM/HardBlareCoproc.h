//===---------------------------- HardBlareMC.h ---------------------------===//
//
//
// HardBlare Project
//
// This file describe ....
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//




class RegisterFiles {

 public:

  enum REG {
    // ARM REGISTER
    R0 = 0,
    R1 = 1,
    R2 = 2,
    R3 = 3,
    R4 = 4,
    R5 = 5,
    R6 = 6,
    R7 = 7,
    R8 = 8,
    R9 = 9,
    R10 = 10,
    R11 = 11,
    R12 = 12,
    R13 = 13,
    R14 = 14,
    R15 = 15,

    // HARDBLARE GENERAL PURPOSE REGISTERS
    SP_VALUE = 16,
    FP_VALUE = 17,
    LR_VALUE = 18,
    HR19 = 19,
    HR20 = 20,
    HR21 = 21,
    HR22 = 22,
    HR23 = 23,

    // CUSTOM REGISTERS
    CR24 = 24,
    CR25 = 25,
    CR26 = 26,
    CR27 = 27,
    CR28 = 28,
    CR29 = 29,

    // ARM SP REGISTER VALUE
    SP = 30,
    // ARM PC REGISTER VALUE
    PC = 31

  };

};







class FPRegisterFiles {

 public:

  enum FPREG {

    FPR0 = 0,
    FPR1 = 1,
    FPR2 = 2,
    FPR3 = 3,
    FPR4 = 4,
    FPR5 = 5,
    FPR6 = 6,
    FPR7 = 7,
    FPR8 = 8,
    FPR9 = 9,
    FPR10 = 10,
    FPR11 = 11,
    FPR12 = 12,
    FPR13 = 13,
    FPR14 = 14,
    FPR15 = 15,
    FPR16 = 16,
    FPR17 = 17,
    FPR18 = 18,
    FPR19 = 19,
    FPR20 = 20,
    FPR21 = 21,
    FPR22 = 22,
    FPR23 = 23,
    FPR24 = 24,
    FPR25 = 25,
    FPR26 = 26,
    FPR27 = 27,
    FPR28 = 28,
    FPR29 = 29,
    FPR30 = 30,
    FPR31 = 31

  };

};



// ********************************************************
// ********************************************************
//
//   Instruction formats for dependencies
//
// ********************************************************
// ********************************************************


enum OPCODE : unsigned {

  // --- Init Format
    LW = 0x01,
    SW = 0x02,
    LCR = 0x03,
    Write_TLB_entry = 0x04,
    LW_FP = 0x05,
    SW_FP = 0x06,
    Tag_rri = 0x07,

    /// ???
    MOV_TRF_2_TRF_FP = 0x08,
    MOV_TFR_FP_2_TRF = 0x09,
    /// ???

    Tag_reg_imm = 0x20,
    Tag_reg_reg = 0x21,
    Tag_mem_reg = 0x22,
    // --- Init Format

    // --- TR
      TRR = 0x23,
      // --- TR

      // --- TI
      Tag_mem_tr = 0x24,
      Tag_tr_mem = 0x25,
      // --- TI

    // --- TR
      Tag_arith_log = 0x26,
    // --- TR

    // --- TI
      Tag_instrumentation_tr = 0x27,
      Tag_tr_instrumentation = 0x28,
      Tag_Kblare_tr = 0x29,
      Tag_tr_Kblare = 0x2a,
    // --- TI

    // --- Init Format
    Lui = 0x2b,
    // --- Init Format

    // --- TI
    Tag_mem_tr_2 = 0x2c,
    Tag_tr_mem_2 = 0x2d,
    // --- TI

    // --- TR
    Tag_check_reg = 0x2e,
    // --- TR

    // --- TI
    Tag_check_mem = 0x2f,
    // --- TI

    // --- Init
    TagFRImm = 0x30,
    TagFRFR = 0x31,
    TagMFR = 0x32,
    // --- Init

    // --- TR
    TagFRFRFR = 0x33,
    // --- TR

    // --- TI
    Tag_mem_tfr = 0x34,
    // --- TI

    // --- TR
    Tag_tfr_mem = 0x35,
    Tag_arith_log_fp = 0x36,
    // --- TR

    // --- TI
    tag_instrumentation_tfr = 0x37,
    tag_tfr_instrumentation = 0x38,
    Tag_kblare_tr_fp = 0x39,
    Tag_tr_fp_kblare = 0x3a,
    // --- TI

    // --- Init
    lui_fp = 0x3b,
    // --- Init

    // --- TI
    Tag_mem_tfr_2 = 0x3c,
    Tag_tfr_mem_2 = 0x3d,
    // --- TI

    // --- TR
    Tag_check_fp = 0x3e,
    // --- TR

    // --- TI
    Tag_check_mem_fp = 0x3f,
    // --- TI

    };


  enum FUNC {

    ADD = 0x00,
    SUB = 0x01,
    OR = 0x02,
    AND = 0x03,
    XOR = 0x04,
    NOR = 0x06,
    copy_src_1 = 0x07,
    copy_src_2 = 0x08,

  };




// ARM Opcode type

enum ARM_OPCODE_TYPE : unsigned {
  MoveOp = 1,
  ArithLogicOp = 2,
  CompOp = 3,
  ArithLogicCompOp = 4,
  FPMoveOp = 5,
  FPArithLogicOp = 6,
  FPCompOp = 7,
  CUSTOM1Op = 8,
  CUSTOM2Op = 9,
  CUSTOM3Op = 10,
  CUSTOM4Op = 11,

  // NOT IMPLEMENTED
  // Operation for Updating Stack Pointer (SP) and Frame Pointer (FP)
  UPDATE_TRACKED_VALUE_REG = 12,

  // Move the value of tracked registers for example : mov sp fp.
  // We have in the monitor the value of SP and FP because we track them
  // but by executing this instruction, we need to update SP with the value of FP
  MOVE_VALUE_TRACKED_REG = 14,

  // CALL RET
  CALL_RET = 15,

  LOAD_STORE = 16
};



// TAG_OP
enum TAG_OP {
  Init = 1,
  Update = 2,
  Check = 3,

  Init_Tag_Val = 4,
  None = 5
};

