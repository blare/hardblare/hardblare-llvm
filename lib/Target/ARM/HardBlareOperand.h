

//===---------------------------- Instruction.h ---------------------------===//
//
//
// HardBlare Project
//
// This file describe ....
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//


#ifndef LLVM_HARDBLARE_OPERAND_H
#define LLVM_HARDBLARE_OPERAND_H

#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/MC/MCContext.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCObjectFileInfo.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOpcodes.h"
#include "llvm/Target/TargetSubtargetInfo.h"
#include "llvm/CodeGen/MachineOperand.h"

#include "llvm/Support/Debug.h"

#include "llvm/ADT/DenseMap.h"
#include <iterator>
#include "llvm/ADT/MapVector.h"
#include "llvm/ADT/SmallVector.h"
#include <map>
#include <vector>

// #include "llvm/Target/MCTargetDesc/ARMAddressingModes.h"

#include "HardBlareRegisterFile.h"


#define DEBUG_TYPE "HardBlare-Operand"

using namespace llvm;

class HardBlareOperand {

 public:

  enum ADDR_MODE {

    // Zero offset
    REGISTER_BASE,                       //             Indexed, base               |           Register indirect            | LDR R0, [R1]

    // Immediate offset
    REGISTER_BASE_OFFSET,                // Pre-indexed	base base with displacement |     Register indirect with offset      | LDR R0, [R1, #4]

    // Register offset
    REGISTER_BASE_REGISTER_BASE,         //          Double Reg indirect            |   Register indirect Register indexed   | LDR R0, [R1, R2]

    // Scaled register offset
    REGISTER_BASE_REGISTER_BASE_SHIFT,    //    Double Reg indirect with scaling     | Register indirect indexed with scaling | LDR R0, [R1, r2, LSL #2]

    // Constant pool
    CONSTANT_POOL

  };

  enum ADDR_MODE_WRITE_BACK : unsigned {
    NONE,
      PRE_INDEX,
      POST_INDEX,
      INCREMENT_AFTER,
      INCREMENT_BEFORE,
      DECREMENT_AFTER,
      DECREMENT_BEFORE
  };

  enum RegisterNumber : unsigned
  {
    // GPR
    R0 = ARM::R0,
      R1 =  ARM::R1,
      R2 = ARM::R2,
      R3 = ARM::R3,
      R4 = ARM::R4,
      R5 = ARM::R5,
      R6 = ARM::R6,
      R7 = ARM::R7,
      R8 = ARM::R8,
      R9 = ARM::R9,
      R10 = ARM::R10,
      R11 = ARM::R11,
      R12 = ARM::R12,

      // Special
      SP = ARM::SP,
      LR = ARM::LR,
      PC = ARM::PC,

      // CPSR Flags
      CPSR_N,
      CPSR_Z,
      CPSR_C,
      CPSR_V,

      CPSR_Q,

      // HardBlare Temporary Variables
      TMP_RESULT,
      TMP_CARRY_OUT,
      TMP_OVERFLOW

      };


  enum TagNumber : unsigned int {

    // Low level predefined tags
    LL_TAG_0 = 0,
      LL_TAG_1 = 1,
      LL_TAG_2 = 2,
      LL_TAG_3 = 3,
      LL_TAG_4 = 4,
      LL_TAG_5 = 5,
      LL_TAG_6 = 6,
      LL_TAG_7 = 7,
      LL_TAG_8 = 8,
      LL_TAG_9 = 9,

      // OS Level predefined tags
      OS_TAG_0 = 20,
      OS_TAG_1 = 21,
      OS_TAG_2 = 22,
      OS_TAG_3 = 23,
      OS_TAG_4 = 24,
      OS_TAG_5 = 25,
      OS_TAG_6 = 26,
      OS_TAG_7 = 27,
      OS_TAG_8 = 28,
      OS_TAG_9 = 29,

      // PC Relative  tags
      PC_RELATIVE_TAG = 42,

      // Constant pool
      CONSTANT_POOL_TAG = 45,

      RETURN_CFI = 89

      };

 public :

  enum OperandType : unsigned char
  {
    Immediate,           /// Immediate operand.
      Register,          /// Register operand.
      Address,           /// Address
      Tag                /// Tag.
      //    CFIOffset             /// CFI with Offset
      // Immediate,         /// Immediate operand.
      // Offset,            ///  Offset.
      // MemBaseRegOffset,  /// Base Register and Offset.
      // Expr,              /// Relocatable immediate operand.
      // Inst,              /// Sub-instruction operand.
      // Tag                /// Tag.
      };

 private :

  OperandType Kind;

  unsigned getRegisterNumber( unsigned _RegNum)
  {

    if(_RegNum == ARM::R0)
      return RegisterNumber::R0;

    else if(_RegNum == ARM::R1)
      return RegisterNumber::R1;

    else if(_RegNum == ARM::R2)
      return RegisterNumber::R2;

    else if(_RegNum == ARM::R3)
      return RegisterNumber::R3;

    else if(_RegNum == ARM::R4)
      return RegisterNumber::R4;

    else if(_RegNum == ARM::R5)
      return RegisterNumber::R5;

    else if(_RegNum == ARM::R6)
      return RegisterNumber::R6;

    else if(_RegNum == ARM::R7)
      return RegisterNumber::R7;

    else if(_RegNum == ARM::R8)
      return RegisterNumber::R8;

    else if(_RegNum == ARM::R9)
      return RegisterNumber::R9;

    else if(_RegNum == ARM::R10)
      return RegisterNumber::R10;

    else if(_RegNum == ARM::R11)
      return RegisterNumber::R11;

    else if(_RegNum == ARM::R12)
      return RegisterNumber::R12;

    else if(_RegNum == ARM::SP)
      return RegisterNumber::SP;

    else if(_RegNum == ARM::LR)
      return RegisterNumber::LR;

    else if(_RegNum == ARM::PC)
      return RegisterNumber::PC;

    else{
      DEBUG( dbgs() << "Error : getRegisterNumber RegisterNumber NOT FOUND \n");
    }

  }




std::string getRegisterNumberToString(unsigned _RegNum) const
  {

    if(_RegNum == RegisterNumber::R0)
      return "R0";

    else if(_RegNum == RegisterNumber::R1)
      return "R1";

    else if(_RegNum == RegisterNumber::R2)
      return "R2";

    else if(_RegNum == RegisterNumber::R3)
      return "R3";

    else if(_RegNum == RegisterNumber::R4)
      return "R4";

    else if(_RegNum == RegisterNumber::R5)
      return "R5";

    else if(_RegNum == RegisterNumber::R6)
      return "R6";

    else if(_RegNum == RegisterNumber::R7)
      return "R7";

    else if(_RegNum == RegisterNumber::R8)
      return "R8";

    else if(_RegNum == RegisterNumber::R9)
      return "R9";

    else if(_RegNum == RegisterNumber::R10)
      return "R10";

    else if(_RegNum == RegisterNumber::R11)
      return "R11";

    else if(_RegNum == RegisterNumber::R12)
      return "R12";

    else if(_RegNum == RegisterNumber::SP)
      return "SP";

    else if(_RegNum == RegisterNumber::LR)
      return "LR";

    else if(_RegNum == RegisterNumber::PC)
      return "PC";

    else if(_RegNum == RegisterNumber::CPSR_N)
      return "CPSR_N";

    else if(_RegNum == RegisterNumber::CPSR_Z)
      return "CPSR_Z";

    else if(_RegNum == RegisterNumber::CPSR_C)
      return "CPSR_C";

    else if(_RegNum == RegisterNumber::CPSR_V)
      return "CPSR_V";

    else{
      DEBUG( dbgs() << "Error : getRegisterNumber RegisterNumber NOT FOUND \n");
      return "?";
    }

  }



 public:

  typedef struct _Address
  {

    // Mode
    ADDR_MODE mode;

    // Write Back Mode
    ADDR_MODE_WRITE_BACK wb_mode;

    // Absolute Address
    int32_t AbsoluteVal;

    // Base Register
    unsigned BaseRegVal;

    // Second Base Register
    unsigned SecondBaseRegVal;

    // Offset Reg
    int32_t OffsetVal;

    // Scalling
    int32_t ScallingVal;

    // CFI Index
    unsigned CFIindex;

  } AddressType;


  union
  {
    AddressType AddressVal;
    unsigned int RegVal;
    int32_t ImmVal;
    unsigned int TagVal;
  };


  HardBlareOperand(OperandType _type, int32_t _imm)
    {

      if(_type == OperandType::Immediate)
        {
          Kind = _type;
          TagVal = _imm;
        }
      else
        {
          assert(true && "Error HardBlareOperand Immediate");
        }
    }


  HardBlareOperand(OperandType _type, TagNumber _tag)
    {

      if(_type == OperandType::Tag)
        {
          Kind = _type;
          TagVal = _tag;
        }
      else
        {
          assert(true && "Error HardBlareOperand Tag");
        }
    }


  HardBlareOperand(OperandType _type, ADDR_MODE_WRITE_BACK _wb_mode, RegisterNumber _reg)
    {

      Kind = _type;

      if(_type == OperandType::Register)
        {
          RegVal = _reg;
        }
      else if(_type == OperandType::Address)
        {
          AddressVal.BaseRegVal = _reg;
          AddressVal.wb_mode = _wb_mode;
        }
      else
        {
          assert(true && "Error HardBlareOperand RegisterNumber _type");
        }
    }


  HardBlareOperand(OperandType _type, ADDR_MODE_WRITE_BACK _wb_mode, MachineOperand& _Op)
    {

      if(_type == OperandType::Address)
        {
          if(_Op.isReg())
            {
              Kind = _type;
              AddressVal.mode = ADDR_MODE::REGISTER_BASE;
              AddressVal.OffsetVal = 0;
              AddressVal.BaseRegVal = getRegisterNumber( _Op.getReg() );
              AddressVal.wb_mode = _wb_mode;
            }

          else if(_Op.isCFIIndex())
            {
              /*
              Kind = OperandType::Address;
              AddressVal.mode = ADDR_MODE::PC_RELATIVE;
              AddressVal.AbsoluteVal = _Base.getCFIIndex();
              */
            }

          else if(_Op.isImm())
            {
              /*
              Kind = OperandType::Address;
              AddressVal.mode = ADDR_MODE::ABSOLUTE;
              AddressVal.AbsoluteVal = _Op.getImm();
              */
            }
        }

      else
        {
          if(_Op.isReg())
            {
              Kind = _type;
              RegVal = getRegisterNumber( _Op.getReg());
            }

          else if(_Op.isImm())
            {
              Kind = _type;
              ImmVal = _Op.getImm();
            }

          else
            {
              assert( false && "[HardBlareOperand Constr 1] Error : 1 Op case not found" );
            }
        }
    }



  HardBlareOperand(OperandType _type, ADDR_MODE_WRITE_BACK _wb_mode, MachineOperand& _Base, MachineOperand& _Op)
    {

      assert( (_type == OperandType::Address)
              && "[HardBlareOperand Constr 2] Constructor not an Address ");

      AddressVal.wb_mode = _wb_mode;

      // REGISTER_BASE_OFFSET
      if( (_Base.isReg()) && (_Op.isImm()) )
        {
          Kind = _type;
          AddressVal.mode = ADDR_MODE::REGISTER_BASE_OFFSET;
          AddressVal.BaseRegVal = getRegisterNumber( _Base.getReg() );
          AddressVal.OffsetVal = _Op.getImm();
        }

      // REGISTER_BASE_REGISTER_BASE
      else if( (_Base.isReg()) && (_Op.isReg()) )
          {
            Kind = _type;
            AddressVal.mode = ADDR_MODE::REGISTER_BASE_REGISTER_BASE;
            AddressVal.BaseRegVal = getRegisterNumber( _Base.getReg() );
            AddressVal.SecondBaseRegVal = getRegisterNumber( _Op.getReg() );
          }

      else
        {
          assert( false && "[HardBlareOperand Constr 2] not found a type ");
        }
    }


  HardBlareOperand(OperandType _type, ADDR_MODE_WRITE_BACK _wb_mode, MachineOperand& _Base, MachineOperand& _Op, MachineOperand& _Op2)
    {

      assert( (_type == OperandType::Address) && " [HardBlareOperand Constr 3] not an Address ");

      AddressVal.wb_mode = _wb_mode;

      // REGISTER_BASE_REGISTER_BASE_SHIFT
      if( (_Base.isReg()) && (_Op.isReg()) && (_Op2.isImm()) )
        {
          Kind = _type;
          AddressVal.mode = ADDR_MODE::REGISTER_BASE_REGISTER_BASE_SHIFT;
          AddressVal.BaseRegVal = getRegisterNumber( _Base.getReg() );
          AddressVal.SecondBaseRegVal = getRegisterNumber( _Op.getReg() );
          AddressVal.ScallingVal = _Op2.getImm();
        }
      else
        {
          assert( false && "[HardBlareOperand Constr 3] not found a type " );
        }

    }


  bool isReg() const { return Kind == Register; }

  bool isImm() const { return Kind == Immediate; }

  bool isAddr() const { return Kind == Address; }

  bool isTag() const { return Kind == Tag; }


  // Redefinition of the operator
  bool operator <(const HardBlareOperand& other) const
  {
    if((this->Kind == Register) && other.isReg())
      {
        return this->RegVal < other.RegVal;
      }
    else
      {
        return false;
      }
  }


 static RegisterNumber getRegisterNumberFromString( std::string RegName)
  {
    if(RegName == "R0")
      return RegisterNumber::R0;

    else if(RegName == "R1")
      return RegisterNumber::R1;

    else if(RegName == "R2")
      return RegisterNumber::R2;

    else if(RegName == "R3")
      return RegisterNumber::R3;

    else if(RegName == "R4")
      return RegisterNumber::R4;

    else if(RegName == "R5")
      return RegisterNumber::R5;

    else if(RegName == "R6")
      return RegisterNumber::R6;

    else if(RegName == "R7")
      return RegisterNumber::R7;

    else if(RegName == "R8")
      return RegisterNumber::R8;

    else if(RegName == "R9")
      return RegisterNumber::R9;

    else if(RegName == "R10")
      return RegisterNumber::R10;

    else if(RegName == "R11")
      return RegisterNumber::R11;

    else if(RegName == "R12")
      return RegisterNumber::R12;

    else if(RegName == "SP")
      return RegisterNumber::SP;

    else if(RegName == "LR")
      return RegisterNumber::LR;

    else if(RegName == "PC")
      return RegisterNumber::PC;

    else if(RegName == "CPSR.N")
      return RegisterNumber::CPSR_N;

    else if(RegName == "CPSR.Z")
      return RegisterNumber::CPSR_Z;

    else if(RegName == "CPSR.C")
      return RegisterNumber::CPSR_C;

    else if(RegName == "CPSR.V")
      return RegisterNumber::CPSR_V;

    else{
      DEBUG( dbgs() << "Error : getRegisterNumber RegisterNumber NOT FOUND \n");
    }
  }


  //
  // Printer
  // ------------------------------------------------

  void print(raw_ostream &OS) const {
    //    OS << "<AnnotationOperand ";
    /*    if (!isValid())
      {
        OS << "INVALID";
      }
      else */
    if (isReg())
      {
        OS << "r" << RegVal;
      }
    else
      {
        OS << "UNDEFINED";
        OS << " ";
      }
  }

  // TODO
  void dump() const
  {

    if( Kind == HardBlareOperand::Register )
      {
        DEBUG( dbgs() << getRegisterNumberToString(RegVal) );
      }

    else if( Kind == HardBlareOperand::Immediate )
      {
        DEBUG( dbgs() << "#" << ImmVal );
      }

    if( Kind == HardBlareOperand::Tag )
      {
        DEBUG( dbgs() << "Tag(" << TagVal << ")" );
      }

    else if( Kind == HardBlareOperand::Address )
      {

        DEBUG( dbgs() << "Address(" );
        switch(AddressVal.wb_mode)
          {

          case (PRE_INDEX) :
            DEBUG( dbgs() << "PRE_INDEX" );
            break;

          case (POST_INDEX) :
            DEBUG( dbgs() << "POST_INDEX" );
            break;

          case (INCREMENT_AFTER) :
            DEBUG(  dbgs() << "INCREMENT_AFTER" );
            break;

          case (INCREMENT_BEFORE) :
            DEBUG(  dbgs() << "INCREMENT_BEFORE" );
            break;

          case (DECREMENT_BEFORE) :
            DEBUG(  dbgs() << "DECREMENT_BEFORE" );
            break;

          case (DECREMENT_AFTER) :
            DEBUG( dbgs() << "DECREMENT_AFTER" );
            break;

          case (NONE) :
            DEBUG( dbgs() << "NONE" );
            break;

          default :
            DEBUG( dbgs() << "NOT_FOUND" );
            break;
          }

        DEBUG( dbgs() << ")" );

        if( (AddressVal.mode == REGISTER_BASE) )
          {
            DEBUG( dbgs() <<
                   AddressVal.mode
                   << "[R"
                   <<  AddressVal.BaseRegVal
                   << "]" );
          }

        else if( (AddressVal.mode == REGISTER_BASE_OFFSET) )
          {
            DEBUG( dbgs() <<
                   AddressVal.mode
                   << "[R"
                   <<  AddressVal.BaseRegVal
                   << ", #"
                   <<  AddressVal.OffsetVal
                   << "]" );
          }

        else if( (AddressVal.mode == REGISTER_BASE_REGISTER_BASE) )
          {
            DEBUG( dbgs() <<
                   AddressVal.mode
                   << "[R"
                   <<  AddressVal.BaseRegVal
                   << ", R"
                   <<  AddressVal.SecondBaseRegVal
                   << "]" );
          }

        else if( (AddressVal.mode == REGISTER_BASE_REGISTER_BASE_SHIFT) )
          {
            DEBUG( dbgs() <<
                   AddressVal.mode
                   << "[R"
                   <<  AddressVal.BaseRegVal
                   << ", R"
                   <<  AddressVal.SecondBaseRegVal
                   << ", "
                   <<  AddressVal.ScallingVal
                   << "]" );

            DEBUG( dbgs() <<
                   AddressVal.mode
                   << "[R"
                   <<  AddressVal.BaseRegVal
                   << ", R"
                   <<  AddressVal.SecondBaseRegVal
                   << ", "
                   <<  AddressVal.ScallingVal
                   << "]" );
          }

        else
          {
            assert( false && "[HardBlareOperand] not found a type ");
          }
      }

  }
};


#endif
