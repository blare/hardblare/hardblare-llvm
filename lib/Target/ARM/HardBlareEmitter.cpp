//===-------------------- Emitter.cpp - HardBlare -----------------------===//
//
//
// HardBlare Project
//
// This file describe ....
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//


#include "llvm/CodeGen/AsmPrinter.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
//#include "llvm/CodeGen/MachineFunction.h"
//#include "llvm/CodeGen/MachineInstr.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/MC/MCContext.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCObjectFileInfo.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOpcodes.h"
#include <iterator>

#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"


#include "HardBlareInformationFlow.h"
#include "HardBlareEmitter.h"

#define DEBUG_TYPE "hardblare-emitter"

using namespace llvm;


/// Emit the HardBlare header.
///
/// Header {
///   uint16  : HardBlare Version
///   uint16  : Representations
///   uint32 : NumBasicBlocks
/// }


void HardBlareEmitter::emitHardBlareHeader(MCStreamer &OS)
{

  // Out context
  MCContext &OutContext = AP.OutStreamer->getContext();

  // Get the HardBlare Informations section.
  MCSection *HardBlareSectionInfo = OutContext.getObjectFileInfo()->getHardBlareInfoSection();

  // Go to the information section
  OS.SwitchSection(HardBlareSectionInfo);

  // Emit HardBlare version
  OS.EmitIntValue(HARDBLARE_VERSION, 4);

  // Emit HardBlare Reprensentation version
  OS.EmitIntValue(HARDBLARE_REPRESENTATION, 4);

  // Debug informations
  DEBUG(dbgs() << "HardBlare Header\n");
  DEBUG(dbgs() << "\tHardBlare version :" << HARDBLARE_VERSION << '\n');
  DEBUG(dbgs() << "\tHardBlare representation :" << HARDBLARE_REPRESENTATION << '\n');

}


/// Serialize the basic block annotations.
void HardBlareEmitter::emitAnnotations(MCStreamer &OS, GlobalInformationFlows* _GIF) {

  // Out context
  MCContext &OutContext = AP.OutStreamer->getContext();
  // Get the HardBlare Annotations section.
  MCSection *HardBlareSectionAnnotations = OutContext.getObjectFileInfo()->getHardBlareAnnotationsSection();
  // Get the HardBlare Basick Block Table section.
  MCSection *HardBlareSectionBasicBlock = OutContext.getObjectFileInfo()->getHardBlareBasicBlockTableSection();
  // Get the text section.
  MCSection *SectionText = OutContext.getObjectFileInfo()->getTextSection();


  // *************************************
  //
  //  Basic Block Table
  //  ------------------
  //  Number of entries
  //
  // *************************************

  // Switch to Basic Block Table Section
  OS.SwitchSection(HardBlareSectionBasicBlock);
  // Number of Basic Blocks
  auto numberOfEntries = _GIF->size();
  // Emit size of annotations
  OS.EmitIntValue(numberOfEntries, 4);


  for(auto GI = _GIF->begin() ; GI != _GIF->end() ; GI++)
    {

    auto FuncIF = GI->second;

    for(auto BB = FuncIF->begin() ; BB != FuncIF->end() ; BB++)
      {

        // The symbol of the basic block
        MCSymbol *PTMlabel = BB->first;

        // *************************************
        //
        // Generate annotation
        // Machine Level representation
        //
        // *************************************
        // Vector of Machine level representation
        AnnotMC depVec;
        // Information Flows vector
        auto vecIF =  BB->second;
        // Emit annotations
        for(auto IF = vecIF->begin(); IF != vecIF->end(); IF++)
          {
            (*IF)->emit(depVec);
          }
        // The number of annotations
        auto numberOfAnnotations = depVec.size();


        DEBUG( dbgs() << "Label : " << PTMlabel->getName() << "\n" );
        DEBUG( dbgs() << "numberOfAnnotations = " << numberOfAnnotations << "\n" );


        // *************************************
        //
        //  Basic Block Table
        //  ------------------
        //  Number of entries
        //  ...
        //  FixUpPTMLabel
        //
        // *************************************

        // Switch to Basic Block Table Section
        OS.SwitchSection(HardBlareSectionBasicBlock);
        // We create a Fixup label regarding the .text section
        const MCExpr *FixUpLabelPTMinTextSectionOffset = MCSymbolRefExpr::create( PTMlabel, MCSymbolRefExpr::VK_ARM_SBREL, OutContext );
        // Emission of the Fixup label
        OS.EmitValue(FixUpLabelPTMinTextSectionOffset, 4);



        // If there is no Annotations for this basic block
        if(numberOfAnnotations == 0)
          {
            // Emit 0x0 in order to specify that this basic block has no annotations
            OS.EmitValue(0, 4);
          }
        else
          {
            // *************************************
            //
            //  Annotations
            //  ------------------
            //  AnnotLabel
            //
            // *************************************

            // Switch to Annotations Section
            OS.SwitchSection(HardBlareSectionAnnotations);
            // Create a Temporary Symbol
            MCSymbol *LabelInAnnotationsSection = OutContext.createTempSymbol();
            // Emit a Label
            OS.EmitLabel(LabelInAnnotationsSection);


            // *************************************
            //
            //  Annotations
            //  ------------------
            //  AnnotLabel
            //  sizeAnnot
            //
            // *************************************

            // Switch to Annotation Section
            OS.SwitchSection(HardBlareSectionAnnotations);
            // Emit number of annotations
            OS.EmitIntValue(numberOfAnnotations, 4);


            // *************************************
            //
            //  Basic Block Table
            //  ------------------
            //  Number of entries
            //  ...
            //  FixUpPTMLabel
            //  FixUpAnnotLabel
            //
            // *************************************

            // Switch to Basic Block Table Section
            OS.SwitchSection(HardBlareSectionBasicBlock);
            // FixUp
            const MCExpr *FixUpLabelAnnotationOffset = MCSymbolRefExpr::create( LabelInAnnotationsSection, MCSymbolRefExpr::VK_ARM_SBREL, OutContext );
            // Emit FixUp
            OS.EmitValue(FixUpLabelAnnotationOffset, 4);


            // *************************************
            //
            //  Annotations
            //  ------------------
            //  AnnotLabel
            //  sizeAnnot
            //  ...
            //  annotations ...
            //  ...
            //
            // *************************************

            // Switch to Annotations Section
            OS.SwitchSection(HardBlareSectionAnnotations);
            // Emit annotations
            for(auto dep = depVec.begin(); dep != depVec.end(); dep++)
              {
                OS.EmitIntValue(*dep, 4);
              }
          }
      }
    }
}


void HardBlareEmitter::emit(MCTargetStreamer &TS) {

  MCContext &OutContext = AP.OutStreamer->getContext();
  MCStreamer &OS = *AP.OutStreamer;

  // Get the singleton class
  auto GIF = GlobalInformationFlowsSingleton::getInstance().getGlobalInformationFlows();

  // Debug
  DEBUG( dbgs() << "\n ============= Emit ============= \n" );
  DEBUG( dbgs() << " instance = " << &(GlobalInformationFlowsSingleton::getInstance()) << " GIF size = " << GIF->size() << " GIF pointer = " << GIF <<  "\n" );

  // Get the HardBlare Informations section.
  MCSection *HardBlareSectionInfo = OutContext.getObjectFileInfo()->getHardBlareInfoSection();

  // Go to the information section
  OS.SwitchSection(HardBlareSectionInfo);

  // Emit the header
  emitHardBlareHeader(OS);

  // Blank line
  OS.AddBlankLine();

  // Emit the annotations
  emitAnnotations(OS, GIF);

  // Blank line
  OS.AddBlankLine();

}


