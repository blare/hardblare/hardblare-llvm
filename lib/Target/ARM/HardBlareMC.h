//===---------------------------- HardBlareMC.h ---------------------------===//
//
//
// HardBlare Project
//
// This file describe ....
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//


#ifndef LLVM_HARDBLARE_MC_H
#define LLVM_HARDBLARE_MC_H

#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/MC/MCContext.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCObjectFileInfo.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOpcodes.h"
#include "llvm/Target/TargetSubtargetInfo.h"
#include "llvm/CodeGen/MachineOperand.h"

#include "llvm/Support/Debug.h"

#include "llvm/ADT/DenseMap.h"
#include <iterator>
#include "llvm/ADT/MapVector.h"
#include "llvm/ADT/SmallVector.h"
#include <map>
#include <vector>

#include "HardBlareRegisterFile.h"
#include "HardBlareOperand.h"
#include "HardBlareCoproc.h"

#include "llvm/HardBlareCommandLine/HardBlareCommandLine.h"

#include <bitset>
#include <string>
#include <iostream>
#include <climits>
#include <future>
#include <mutex>


#define DEBUG_TYPE "HardBlare-MC"

using namespace llvm;


// Annotations Low Level
// ============================================
typedef SmallVector<unsigned long, 1500> AnnotMC;


class SecondStageDIFT {

 public:

  SecondStageDIFT() {}

  virtual void emit( AnnotMC& depVec ) {}
  virtual void dump() {}

};



//
// Init format used for initialization of register and memory tags
// ----------------------------------------------------------------

class ForamtInit : public SecondStageDIFT {

 protected:

  std::bitset<6> OPCODE;

  std::bitset<5> reg_dst;
  std::bitset<5> reg_src;

  std::bitset<16> Immediate;

 public:

 ForamtInit( enum OPCODE _op, unsigned _reg_dst, unsigned _reg_src, unsigned _imm )
   : OPCODE(_op),
    reg_dst(_reg_dst),
    reg_src(_reg_src),
    Immediate(_imm)
    {
    }

  void emit( AnnotMC& depVec )
  {

    std::bitset<32> result( OPCODE.to_string()
                            + reg_dst.to_string()
                            + reg_src.to_string()
                            + Immediate.to_string() );

    depVec.push_back(result.to_ulong());

  }


  void dump()
  {

    DEBUG( dbgs()
           << OPCODE.to_string()
           << reg_dst.to_string()
           << reg_src.to_string()
           << Immediate.to_string() 
           << "\n" );

  }

};






//
// TR (Tag Register Format) Used for upodating tags of registers
// ----------------------------------------------------------------

class FormatTR : public SecondStageDIFT {

 protected:

  std::bitset<6> OPCODE;

  std::bitset<4> ARM_OPCODE_TYPE;

  std::bitset<5> reg_dst;
  std::bitset<5> reg_src;
  std::bitset<5> reg_src2;

  std::bitset<2> null;

  std::bitset<5> FUNC;

 public:

 FormatTR( enum OPCODE _op, enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _reg_src2, unsigned _FUNC )
   : OPCODE(_op),
    ARM_OPCODE_TYPE(_ARM_OPCODE_TYPE),
    reg_dst( _reg_dst),
    reg_src(_reg_src),
    reg_src2(_reg_src2),
    FUNC(_FUNC)
    {
    }


  void emit( AnnotMC& depVec )
  {

    std::bitset<32> result( OPCODE.to_string()
                            + ARM_OPCODE_TYPE.to_string()
                            + reg_dst.to_string()
                            + reg_src.to_string()
                            + reg_src2.to_string()
                            + null.to_string()
                            + FUNC.to_string() );

    depVec.push_back(result.to_ulong());

  }


  void dump()
  {

    DEBUG( dbgs()
           << OPCODE.to_string()
           << ARM_OPCODE_TYPE.to_string()
           << reg_dst.to_string()
           << reg_src.to_string()
           << reg_src2.to_string()
           << null.to_string()
           << FUNC.to_string()
           << "\n" );

  }


};


//
// TI Format used for updating tags of memory
// ----------------------------------------------------------------

class ForamtTI : public SecondStageDIFT {

 protected:

  std::bitset<6> OPCODE;

  std::bitset<4> ARM_OPCODE_TYPE;

  std::bitset<5> reg_dst;
  std::bitset<5> reg_src;

  std::bitset<12> Immediate;

 public:

 ForamtTI( enum OPCODE _op, unsigned _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _Immediate )
   : OPCODE(_op),
    ARM_OPCODE_TYPE(_ARM_OPCODE_TYPE),
    reg_dst( _reg_dst),
    reg_src(_reg_src),
    Immediate(_Immediate)
    {
    }


  void emit( AnnotMC& depVec )
  {

    std::bitset<32> result( OPCODE.to_string()
                            + ARM_OPCODE_TYPE.to_string()
                            + reg_dst.to_string()
                            + reg_src.to_string()
                            + Immediate.to_string() );

    depVec.push_back(result.to_ulong());

  }


  void dump()
  {

    DEBUG( dbgs()
           << OPCODE.to_string()
           << ARM_OPCODE_TYPE.to_string()
           << reg_dst.to_string()
           << reg_src.to_string()
           << Immediate.to_string()
           << "\n" );

  }

};







//
//  LW (init format)
//  -----------------
//  reg_dst = Mem(reg_src + o set)
//  general purpose instruction to load a value from an address value stored in a register.
//

class annot_LW : public ForamtInit {

 public:

 annot_LW(unsigned _reg_dst, unsigned _reg_src, unsigned _imm) : ForamtInit( OPCODE::LW, _reg_dst, _reg_src, _imm ) {}

};

//
//  SW (init format)
//  -----------------
//  Mem(reg_dst + o set) = reg_src
//  general purpose instruction to store a value to a memory address.
//

class annot_SW : public ForamtInit {

 public:

annot_SW(unsigned _reg_dst, unsigned _reg_src, unsigned _imm) : ForamtInit( OPCODE::SW, _reg_dst, _reg_src, _imm ) {}

};


//
//  LCR (init format)
//  -----------------
//  reg_dst_con g_register = Mem(reg_src + o set)
//  Load configuration register value from an address.
//

class annot_LCR : public ForamtInit {

 public:

 annot_LCR(unsigned _reg_dst, unsigned _reg_src, unsigned _imm) : ForamtInit( OPCODE::LCR, _reg_dst, _reg_src, _imm ) {}

};



//
//  Write a TLB entry (init format)
//  -----------------
//  TLB[auto] = reg_src.
//  Initialize a TLB entry corresponding to an address given in reg_src entry.
//

class annot_TLBentry : public ForamtInit {

 public:

 annot_TLBentry(unsigned _reg_src) : ForamtInit( OPCODE::Write_TLB_entry, 0x0, _reg_src, 0x0 ) {}

};



//
//  Tag_rri (init format)
//  -------------------------
//  reg_dst = imm initialize reg with an immediate value: this
//  instruction can be used to initialize tag values or register values ...
//

class annot_Tag_rri : public ForamtInit {

 public:

 annot_Tag_rri(unsigned _reg_dst, unsigned _reg_src, unsigned Immediate): ForamtInit( OPCODE::Tag_rri,  _reg_dst, _reg_src, Immediate ) {}

};



//
//  Tag_reg_imm (init format)
//  -------------------------
//  reg_dst = imm initialize reg with an immediate value: this
//  instruction can be used to initialize tag values or register values ...
//

class annot_Tag_reg_imm : public ForamtInit {

 public:

 annot_Tag_reg_imm(unsigned _reg_dst, unsigned _reg_src, unsigned Immediate): ForamtInit( OPCODE::Tag_reg_imm,  _reg_dst, _reg_src, Immediate ) {}

};




//
//  Tag_reg_reg tag (init format)
//  -------------------------
//  reg_dst <= reg_src
//  initialize reg_dst with a value stored in register
//  Move from GPR (of second stage) to tag register file (register 0 to 16)
//

class annot_Tag_reg_reg : public ForamtInit {

 public:

 annot_Tag_reg_reg(unsigned _reg_dst, unsigned _reg_src, unsigned Immediate): ForamtInit( OPCODE::Tag_reg_reg,  _reg_dst, _reg_src, Immediate ) {}

};



//
//  Tag_mem_reg (init format)
//  -------------------------
//  Mem([reg1_dst + o set] ) = reg2
//  initialize memory with a value stored in register
//

class annot_Tag_mem_reg : public ForamtInit {

 public:

 annot_Tag_mem_reg(unsigned _reg_dst, unsigned _reg_src, unsigned Immediate): ForamtInit( OPCODE::Tag_mem_reg,  _reg_dst, _reg_src, Immediate ) {}

};





//
//  LuI (init format)
//  -------------------------
//  reg_dst <= (imm_16 16) | reg_src;
//  To ease initialize of 32 bit values, we can use this instruction
//  to load upper 16 bits into a register with an immediate value
//

class annot_LuI : public ForamtInit {

 public:

 annot_LuI(unsigned _reg_dst, unsigned _reg_src, unsigned Immediate): ForamtInit( OPCODE::Lui,  _reg_dst, _reg_src, Immediate ) {}

};



//
//  TRR (TR format)
//  -------------------------
//  tag(reg_dst) = tag (reg_src1) OPERATION tag(reg_src2)update tag of
//  reg_dst_arm with reg_src1_arm and reg_src2_arm Conditions on
//  instruction operands
//
//

class annot_TRR : public FormatTR {

 public:

 annot_TRR( enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _reg_src2, unsigned _FUNC): FormatTR( OPCODE::TRR, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _reg_src2, _FUNC ) {}

};




//
//  Tag_mem_tr (TI format)
//  -------------------------
//  tag (Mem(reg_dst + o set)) = tag(reg_src1)
//  update tag of memory address contained in reg_dst with tag 
//  value contained in reg_src1 (ARM STR opera- tion)
//  propagate operation speci ed by security policy
//
//

class annot_Tag_mem_tr : public ForamtTI {

 public:

 annot_Tag_mem_tr(  enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _Immediate): ForamtTI( OPCODE::Tag_mem_tr, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _Immediate ) {}

};


//
//  Tag_tr_mem (TI format)
//  -------------------------
//  tag(reg_dst) = tag (Mem(reg_src + o set))
//  update tag of reg_dst with the tag of memory address contained 
//  in (val(reg_src) + o set) (this operation is done for ARM LDR operation)
//  propagate operation speci ed by security policy
//
//

class annot_Tag_tr_mem : public ForamtTI {

 public:

 annot_Tag_tr_mem( unsigned _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _Immediate): ForamtTI( OPCODE::Tag_tr_mem, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _Immediate ) {}

};




//
//  Tag_mem_tr_2 (TI format)
//  -------------------------
//  tag (Mem(reg_dst + o set)) = tag(reg_src1)
//  update tag of memory address contained in reg_dst with tag 
//  value contained in reg_src1 (ARM STR opera- tion)
//  propagate operation speci ed by opcode
//
//

class annot_Tag_mem_tr_2 : public ForamtTI {

 public:

  annot_Tag_mem_tr_2( enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _Immediate): ForamtTI( OPCODE::Tag_mem_tr_2, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _Immediate ) {}

};




//
//  Tag_tr_mem_2 (TI format)
//  -------------------------
//  tag(reg_dst) = tag (Mem(reg_src + o set))
//  update tag of reg_dst with the tag of memory address contained in 
//  (val(reg_src) + o set) (this operation is done for ARM LDR operation)
//  propagate operation speci ed by opcode
//

class annot_Tag_tr_mem_2 : public ForamtTI {

 public:

 annot_Tag_tr_mem_2( enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _Immediate): ForamtTI( OPCODE::Tag_tr_mem_2, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _Immediate ) {}

};




//
//  Tag_arith_log (TR format)
//  -------------------------
//  General purpose operations on GPR of DIFT coprocessor (second stage)
//  (add/or/and/sub/sll/srl ...)
//

class annot_Tag_arith_log : public FormatTR {

 public:

 annot_Tag_arith_log(enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _reg_src2, unsigned _FUNC): FormatTR( OPCODE::Tag_arith_log, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _reg_src2, _FUNC ) {}

};






//
//  Tag_instrumentation_tr (TI format)
//  -------------------------
//  tag(Mem(instrumentation)) <= tag (reg_src1) 
//  (operation done for ARM STR register relative instructions)
//

class annot_Tag_instrumentation_tr : public ForamtTI {

 public:

 annot_Tag_instrumentation_tr( enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _Immediate): ForamtTI( OPCODE::Tag_instrumentation_tr, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _Immediate ) {}

};






//
//  Tag_tr_instrumentation (TI format)
//  -------------------------
//  reg_dst <= tag(Mem(instrumentation)) (opreation done for ARM register relative LDR instructions)
//  Update tag of memory address, obtained by instrumentation IP, with the tag of ARM register (reg_src1)
//

class annot_Tag_tr_instrumentation : public ForamtTI {

 public:

annot_Tag_tr_instrumentation( enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _Immediate): ForamtTI( OPCODE::Tag_tr_instrumentation, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _Immediate ) {}

};





//
//  Tag_Kblare_tr (TI format)
//  -------------------------
//  Mem(KBlare) <= tag (reg_src1)
//  write tag to Kblare IP PL2PS
//

class annot_Tag_Kblare_tr : public ForamtTI {

 public:

 annot_Tag_Kblare_tr( enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _Immediate): ForamtTI( OPCODE::Tag_Kblare_tr, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _Immediate ) {}

};




//
//  Tag_tr_Kblare (TI format)
//  -------------------------
//  tag (reg_dest) <= Mem(KBlare)
//  read tag from Kblare IP PS2PL
//

class annot_Tag_tr_Kblare : public ForamtTI {

 public:

 annot_Tag_tr_Kblare( enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _Immediate): ForamtTI( OPCODE::Tag_tr_Kblare, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _Immediate ) {}

};


//
//  Tag_check_reg (TR format)
//  -------------------------
//  Check tag value of speci ed registers
//

class annot_Tag_check_reg : public FormatTR {

 public:

 annot_Tag_check_reg(enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _reg_src2, unsigned _FUNC): FormatTR( OPCODE::Tag_check_reg, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _reg_src2, _FUNC ) {}

};


//
//  Tag_check_mem (TI format)
//  -------------------------
//  Check tag value of mem address specified 
//  in register (reg_src1 + o set)
//

class annot_Tag_check_mem : public ForamtTI {

 public:

 annot_Tag_check_mem( enum ARM_OPCODE_TYPE _ARM_OPCODE_TYPE, unsigned _reg_dst, unsigned _reg_src, unsigned _Immediate): ForamtTI( OPCODE::Tag_check_mem, _ARM_OPCODE_TYPE, _reg_dst, _reg_src, _Immediate ) {}

};

#endif
