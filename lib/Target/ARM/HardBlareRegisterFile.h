
//===------------------- HardBlareInfo.h - HardBlare ------------------------------===//
//
//
// HardBlare Project
//
// This file describe information about the HardBlare co-processor
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//


#ifndef LLVM_HARDBLARE_REGISTER_FILE_H
#define LLVM_HARDBLARE_REGISTER_FILE_H


#include "ARM.h"
#include "ARMBaseInstrInfo.h"
#include "ARMBaseRegisterInfo.h"
#include "ARMMachineFunctionInfo.h"
#include <string>

using namespace llvm;

class HBRegisterFile {

 public:
   enum Register{

    // GPR
    R0 = 0,
    R1 = 1,
    R2 = 2,
    R3 = 3,
    R4 = 4,
    R5 = 5,
    R6 = 6,
    R7 = 7,
    R8 = 8,
    R9 = 9,
    R10 = 10,
    R11 = 11,
    R12 = 12,

    // Special
    SP = 13,
    LR = 14,
    PC = 15,

    // CPSR Flags
    CPSR_N = 16,
    CPSR_Z = 17,
    CPSR_C = 18,
    CPSR_V = 19,
    CPSR_Q = 20,

    // HardBlare Temporary Variables
    TMP_RESULT = 21,
    TMP_CARRY_OUT = 22,
    TMP_OVERFLOW = 23

  };



  Register getHBRegister(unsigned reg){

    switch(reg){

    case ARM::R0 :
      return Register::R0;
      break;

    case ARM::R1:
      return Register::R1;
      break;

    case ARM::R2:
      return Register::R2;
      break;

    case ARM::R3:
      return Register::R3;
      break;

    case ARM::R4:
      return Register::R4;
      break;

    case ARM::R5:
      return Register::R5;
      break;

    case ARM::R6:
      return Register::R6;
      break;

    case ARM::R7:
      return Register::R7;
      break;

    case ARM::R8:
      return Register::R8;
      break;

    case ARM::R9:
      return Register::R9;
      break;

    case ARM::R10:
      return Register::R10;
      break;

    case ARM::R11:
      return Register::R11;
      break;

    case ARM::R12:
      return Register::R12;
      break;

    case ARM::SP:
      return Register::SP;
      break;

    case ARM::LR:
      return Register::LR;
      break;

    case ARM::PC:
      return Register::PC;
      break;

    case 16:
      return Register::CPSR_N;
      break;

    case 17:
      return Register::CPSR_Z;
      break;

    case 18:
      return Register::CPSR_C;
      break;

    case 19:
      return Register::CPSR_V;
      break;

    case 20:
      return Register::CPSR_Q;
      break;

    case 21:
      return Register::TMP_RESULT;
      break;

    case 22:
      return Register::TMP_CARRY_OUT;
      break;

    case 23:
      return Register::TMP_OVERFLOW;
      break;

      //    case 19:
    default:
      llvm_unreachable("Unknown register");
    }
  }


  Register R;

  HBRegisterFile(unsigned Reg){
    R = getHBRegister(Reg);
  }

  HBRegisterFile(Register Reg){
    R = Reg;
  }


unsigned getRegUnsigned() const {

    switch(R) {
    case R0 :
      return 0;
      break;

    case R1 :
      return 1;
      break;

    case R2 :
      return 2;
      break;

    case R3  :
      return 3;
      break;

    case R4  :
      return 4;
      break;

    case R5 :
      return 5;
      break;

    case R6  :
      return 6;
      break;

    case R7  :
      return 7;
      break;

    case R8  :
      return 8;
      break;

    case R9  :
      return 9;
      break;

    case R10 :
      return 10;
      break;

    case R11 :
      return 11;
      break;

    case R12 :
      return 12;
      break;

    case SP :
      return 13;
      break;

    case LR :
      return 14;
      break;

    case PC :
      return 15;
      break;

    case CPSR_N :
      return 16;
      break;

    case CPSR_Z :
      return 17;
      break;

    case CPSR_C  :
      return 18;
      break;

    case CPSR_V :
      return 19;
      break;

    case CPSR_Q :
      return 20;
      break;

    case TMP_RESULT :
      return 21;
      break;

    case TMP_CARRY_OUT :
      return 22;
      break;

    case TMP_OVERFLOW :
      return 23;
      break;
    }
  }

};


#endif
