//===------------------- StaticAnalysis.cpp - HardBlare -------------------------===//
//
//
// HardBlare Project
//
// This pass implements a static data flow analysis for HardBlare
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//


#ifndef LLVM_HARDBLARE_ANALYSIS
#define LLVM_HARDBLARE_ANALYSIS

#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetInstrInfo.h"
#include "llvm/Target/TargetRegisterInfo.h"
#include "llvm/Target/TargetSubtargetInfo.h"
#include "HardBlareRegisterFile.h"

#include "HardBlareInformationFlow.h"
#include "HardBlareInstrumentation.h"

#include <vector>


using namespace llvm;

class HardBlareAnalysis {

 public:

  static void SimplificationTagOpAnalysis(GlobalInformationFlows& _GlobalInformationFlows, MachineFunction &MF);

  static void ModifyRegsAnalysis(GlobalInformationFlows& _GlobalInformationFlows, MachineFunction &MF);

  static void GenerateInstrumentation(GlobalInformationFlows& _GlobalInformationFlows, MachineFunction &MF);

};

#endif
