//===----------------------- HardBlareInstrumentation.cpp - HardBlare -------------------===//
//
//
// HardBlare Project
//
// This file describe ....
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//


#include "ARMInstrInfo.h"
#include "ARM.h"
#include "ARMBaseInstrInfo.h"
#include "ARMBaseRegisterInfo.h"
#include "ARMConstantPoolValue.h"
#include "ARMFeatures.h"
#include "ARMHazardRecognizer.h"
#include "ARMMachineFunctionInfo.h"
#include "ARMTargetMachine.h"

#include "MCTargetDesc/ARMAddressingModes.h"
#include "MCTargetDesc/ARMMCExpr.h"
#include "MCTargetDesc/ARMBaseInfo.h"

#include "llvm/ADT/STLExtras.h"
#include "llvm/CodeGen/LiveVariables.h"
#include "llvm/CodeGen/MachineConstantPool.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineJumpTableInfo.h"
#include "llvm/CodeGen/MachineMemOperand.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/SelectionDAGNodes.h"
#include "llvm/CodeGen/TargetSchedule.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalValue.h"

#include "llvm/MC/MCAsmInfo.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCSymbol.h"
#include "llvm/MC/MCContext.h"
#include "llvm/MC/ConstantPools.h"
#include "llvm/MC/MCInst.h"

#include "llvm/IR/DerivedTypes.h"

#include "llvm/Support/BranchProbability.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/raw_ostream.h"

#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOpcodes.h"
#include "llvm/Target/TargetRegisterInfo.h"
#include "llvm/Target/TargetSubtargetInfo.h"


#include "Thumb1InstrInfo.h"
#include "ARMSubtarget.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineMemOperand.h"
#include "llvm/MC/MCInst.h"


#include <iterator>

#include "HardBlareRegisterFile.h"
#include "HardBlareInstrumentation.h"

using namespace llvm;


void HardBlareInstrumentation::setHardBlareAddrtoR9(MachineFunction &MF)
{

  auto HARDBLARE_ADDR_REG = ARM::R9;

  MachineRegisterInfo &MRI = MF.getRegInfo();
  const TargetMachine &TM = MF.getTarget();
  MachineMemOperand *CPMMO;
  ARMFunctionInfo AFI = *MF.getInfo<ARMFunctionInfo>();
  unsigned LabelId = AFI.createPICLabelUId();
  const ARMInstrInfo &TII = *static_cast<const ARMInstrInfo*>(MF.getTarget().getMCInstrInfo());

  MachineFunction::iterator first_MBB = MF.begin();
  MachineBasicBlock::instr_iterator MI = first_MBB->instr_begin();
  MachineBasicBlock::instr_iterator MI_end = first_MBB->instr_end();

  MachineInstrBuilder MIB;

  const Function *F = MF.getFunction();
  const Module *M = F->getParent();
  GlobalValue *GV = M->getGlobalVariable("__hardblare_instr_addr");


  while( (MI->getFlag(MachineInstr::FrameSetup)) && (MI != MI_end) )
    {
      if((MI->getFlag(MachineInstr::HardBlareInstrumentationSetup)))
        {
          // Nothing to do (There is already an Instrumentation
          // Setup for this Machine Function)
          return;
        }
      else
        {
          MI++;
        }
    }

  BuildMI(*first_MBB, MI, MI->getDebugLoc(), TII.get(ARM::MOVi16_ga_pcrel),  HARDBLARE_ADDR_REG)
    .addGlobalAddress(GV, 0,  ARMII::MO_LO16)
    .addImm(LabelId)
    .setMIFlag(MachineInstr::HardBlareInstrumentationSetup);

  BuildMI(*first_MBB, MI, MI->getDebugLoc(), TII.get(ARM::MOVTi16_ga_pcrel),  HARDBLARE_ADDR_REG)
    .addReg(HARDBLARE_ADDR_REG)
    .addGlobalAddress(GV, 0,  ARMII::MO_HI16)
    .addImm(LabelId)
    .setMIFlag(MachineInstr::HardBlareInstrumentationSetup);

  BuildMI(*first_MBB, MI, MI->getDebugLoc(), TII.get(ARM::PICLDR),  HARDBLARE_ADDR_REG)
    .addReg(HARDBLARE_ADDR_REG)
    .addImm(LabelId)
    .addImm(ARMCC::AL)
    .addReg(0)
    .setMIFlag(MachineInstr::HardBlareInstrumentationSetup);

}




void HardBlareInstrumentation::sendValueToPL(HardBlareDynamicInformationFlow* DIF, const ARMInstrInfo &TII)
{

  DEBUG( dbgs() << " sendValueToPL 0 \n" );
  auto HARDBLARE_ADDR_REG = ARM::R9;

  DEBUG( dbgs() << " sendValueToPL 1 \n" );
  MachineInstr* MI = DIF->MI;

  DEBUG( dbgs() << " sendValueToPL 2 \n" );
  auto Desc =  MI->getDesc();

  DEBUG( dbgs() << " sendValueToPL 3 \n" );
  MachineBasicBlock *MBB = MI->getParent();

  DEBUG( dbgs() << " sendValueToPL 4 \n" );
  MachineInstrBuilder MIB;

  std::set<HardBlareOperand>::iterator InterestingOp;
  int numberOfInterestingOp = 0;

  for(auto it = DIF->Outs.begin(); it!=DIF->Outs.end(); ++it)
    {
      if(it->isAddr())
        {
          if(numberOfInterestingOp < 1)
            {
              InterestingOp = it;
              numberOfInterestingOp++;
            }
          else
            {
              assert( false && "Instrumentation Outs numberOfInterestingOp > 1 \n ");
            }
        }
    }

  for(auto it = DIF->Ins.begin(); it!=DIF->Ins.end(); ++it)
    {
      if(it->isAddr())
        {
          if(numberOfInterestingOp < 1)
            {
              InterestingOp = it;
              numberOfInterestingOp++;
            }
          else
            {
              assert( false && "Instrumentation Ins numberOfInterestingOp > 1 \n ");
            }
        }
    }

  if( (numberOfInterestingOp == 1) && (InterestingOp->isAddr()) )
    {

      switch(InterestingOp->AddressVal.mode)
        {

        case(HardBlareOperand::REGISTER_BASE) :
          {
            DEBUG(dbgs() << "\n Instrumentation : sendValueToPL (REGISTER_BASE) \n");

            unsigned InterestingReg = InterestingOp->AddressVal.BaseRegVal;

            BuildMI(*MBB, MI, MI->getDebugLoc(), TII.get(ARM::STRi12), InterestingReg)
              .addReg(HARDBLARE_ADDR_REG)
              .addImm(0)
              .addImm(ARMCC::AL)
              .addReg(0)
              .setMIFlag(MachineInstr::HardBlareInstrumentation);

            break;
          }

        case(HardBlareOperand::REGISTER_BASE_OFFSET) :
          {
            DEBUG(dbgs() << "\n Instrumentation : sendValueToPL (REGISTER_BASE_OFFSET) \n");

            unsigned InterestingReg = InterestingOp->AddressVal.BaseRegVal;

            BuildMI(*MBB, MI, MI->getDebugLoc(), TII.get(ARM::STRi12), InterestingReg)
              .addReg(HARDBLARE_ADDR_REG)
              .addImm(0)
              .addImm(ARMCC::AL)
              .addReg(0)
              .setMIFlag(MachineInstr::HardBlareInstrumentation);

            break;
          }

        case(HardBlareOperand::REGISTER_BASE_REGISTER_BASE) :
          {
            DEBUG(dbgs() << "\n Instrumentation : sendValueToPL (REGISTER_BASE_REGISTER_BASE) \n");

            unsigned InterestingReg = InterestingOp->AddressVal.BaseRegVal;
            unsigned InterestingReg2 = InterestingOp->AddressVal.SecondBaseRegVal;

            BuildMI(*MBB, MI, MI->getDebugLoc(), TII.get(ARM::STMIA))
              .addReg(HARDBLARE_ADDR_REG, getDefRegState(false))  // No Write Back
              // .addImm(0)
              .addImm(ARMCC::AL)
              .addReg(0)
              .addReg(InterestingReg)
              .addReg(InterestingReg2)
              .setMIFlag(MachineInstr::HardBlareInstrumentation);

            break;
          }

        case(HardBlareOperand::REGISTER_BASE_REGISTER_BASE_SHIFT) :
          {

            DEBUG(dbgs() << "\n Instrumentation : sendValueToPL (REGISTER_BASE_REGISTER_BASE_SHIFT) \n");

            unsigned InterestingReg = InterestingOp->AddressVal.BaseRegVal;
            unsigned InterestingReg2 = InterestingOp->AddressVal.SecondBaseRegVal;

            std::vector<unsigned> InterestingRegList = {InterestingReg, InterestingReg2};
            std::sort(InterestingRegList.begin(), InterestingRegList.end());

            auto MIB = BuildMI(*MBB, MI, MI->getDebugLoc(), TII.get(ARM::STMIA))
              .addReg(HARDBLARE_ADDR_REG, getDefRegState(false))  // No Write Back
              .addImm(ARMCC::AL)
              .addReg(0);
            for(auto Op = InterestingRegList.begin(); Op != InterestingRegList.end(); Op++ )
              {
                MIB.addReg(*Op);
              }
            MIB.setMIFlag(MachineInstr::HardBlareInstrumentation);

            break;

          }
        default:
          {
            DEBUG(dbgs() << "\n NO Instrumentation : sendValueToPL NOT FOUND \n");
            break;
          }

        }
    }
}


