//===------------------- HardBlareEmitter - HardBlare -------------------------===//
//
//
// HardBlare Project
//
// This pass implements all needed structure and is the entry point for all HardBlare
// Passes and Transformations.
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===---------------------------------------------------------------------------===//

#include "ARM.h"
#include "ARMMachineFunctionInfo.h"

#include "llvm/Pass.h"
#include "llvm/CodeGen/MachineBasicBlock.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetInstrInfo.h"
#include "llvm/Target/TargetRegisterInfo.h"
#include "llvm/Target/TargetSubtargetInfo.h"

#include "HardBlareRegisterFile.h"

#include "HardBlareInformationFlow.h"
#include "HardBlareInstrumentation.h"
#include "HardBlareAnalysis.h"

#include <vector>

#define DEBUG_TYPE "hardblare-pass"

#include "llvm/HardBlareCommandLine/HardBlareCommandLine.h"

#define HARDBLARE_VERSION 2
#define HARDBLARE_REPRESENTATION 2

using namespace llvm;

namespace {

  class MCExpr;
  class MCStreamer;

  struct HardBlarePass : public MachineFunctionPass {
  public:
    static char ID;
    HardBlarePass() : MachineFunctionPass(ID) {}
    bool runOnMachineFunction(MachineFunction &MF) override;
  };
  char HardBlarePass::ID = 0;

}


bool HardBlarePass::runOnMachineFunction(MachineFunction &MF) {


  DEBUG( dbgs() << "\n ============= HardBlare PASS Machine Function ============= \n" );
  DEBUG( dbgs() << "                  " << MF.getName() << "                     \n" );
  DEBUG( dbgs() << "\n =========================================================== \n" );

  auto GIF = GlobalInformationFlowsSingleton::getInstance().getGlobalInformationFlows();

  HardBlareInstrumentation::setHardBlareAddrtoR9(MF);

  DEBUG( dbgs() << "\n ============= HardBlare Pass ============= \n" );
  DEBUG( dbgs() << " instance = " << &(GlobalInformationFlowsSingleton::getInstance()) << "GIF size = " << GIF->size() << " GIF pointer = " << GIF <<  "\n" );

  // Generate Information Flows
  HardBlareInformationFlow::GenerateInformationFlows(MF, *GIF);

  DEBUG( dbgs() << " ----------------------   Before Optim/Simplifcation Pass   ---------------------------- \n" );

  // Before Optim/Simplifcation Pass
  HardBlareInformationFlow::dump(*GIF);

  // Simplification
  HardBlareAnalysis::SimplificationTagOpAnalysis(*GIF, MF);


  DEBUG( dbgs() << " ----------------------   Modify Regs Analysis Pass   ---------------------------- \n" );

  HardBlareAnalysis::ModifyRegsAnalysis(*GIF, MF);

  DEBUG( dbgs() << " ----------------------   Flows Before Instrumentation Analysis Pass   ---------------------------- \n" );

  // Before Optim/Simplifcation Pass
  HardBlareInformationFlow::dump(*GIF);

  DEBUG( dbgs() << " ----------------------   Instrumentation Analysis Pass   ---------------------------- \n" );

  HardBlareAnalysis::GenerateInstrumentation(*GIF, MF);

  DEBUG( dbgs() << " ----------------------  Final Flows  ---------------------------- \n" );

  // Before Optim/Simplifcation Pass
  HardBlareInformationFlow::dump(*GIF);

  DEBUG( dbgs() << " ----------------------  Final Code    ---------------------------- \n" );

  for(auto BB = MF.begin(); BB != MF.end(); BB++)
    {
      for(auto I = BB->begin(); I != BB->end(); I++)
        {
          I->dump();
        }
    }

  return true;

}


FunctionPass *llvm::createHardBlarePass() {
  return new HardBlarePass();
}
