//===------------------- ARMSemantic.h - HardBlare -------------------------===//
//
//
// HardBlare Project
//
// This file describe the semantic of each machine code instruction on ARM
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_HARDBLARE_INSTRUMENTATION_H
#define LLVM_HARDBLARE_INSTRUMENTATION_H

#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCObjectFileInfo.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/CodeGen/MachineOperand.h"


#include "ARMBaseInstrInfo.h"
#include "ARMRegisterInfo.h"

namespace llvm{

  class HardBlareInstrumentation {

  public:

    static void setHardBlareAddrtoR9(MachineFunction &MF);

    static void sendValueToPL(HardBlareDynamicInformationFlow* DIF, const ARMInstrInfo &TII);

  };

}

#endif
