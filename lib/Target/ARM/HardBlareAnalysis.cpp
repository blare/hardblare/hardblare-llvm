//===------------------- StaticAnalysis.cpp - HardBlare -------------------------===//
//
//
// HardBlare Project
//
// This pass implements a static data flow analysis for HardBlare
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//

#include "ARM.h"
#include "ARMMachineFunctionInfo.h"

#include "llvm/Pass.h"
#include "llvm/CodeGen/MachineBasicBlock.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetInstrInfo.h"
#include "llvm/Target/TargetRegisterInfo.h"
#include "llvm/Target/TargetSubtargetInfo.h"
#include "llvm/MC/MCContext.h"

#include "HardBlareRegisterFile.h"

#include "HardBlareInformationFlow.h"
#include "HardBlareInstrumentation.h"
#include "HardBlareAnalysis.h"

// For the Analaysis
#include "llvm/CodeGen/LiveVariables.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/CodeGen/MachineDominators.h"
#include "llvm/CodeGen/MachinePostDominators.h"

#include <vector>

#define DEBUG_TYPE "hardblare-analysis"

using namespace llvm;




void HardBlareAnalysis::SimplificationTagOpAnalysis(GlobalInformationFlows& _GlobalInformationFlows, MachineFunction &MF)
{

  DEBUG(errs() << " ************************************************************* \n");
  DEBUG(errs() << "                   SimplificationTagOpAnalysis                 \n");
  DEBUG(errs() << " ************************************************************* \n");

  // For all basic blocks
  //  for(auto GI = _GlobalInformationFlows.begin(); GI != _GlobalInformationFlows.end(); GI++)
  //  {

  auto GI = _GlobalInformationFlows.find(MF.getFunctionNumber());

  DEBUG( dbgs() << "\n\n Function ID: " << MF.getFunctionNumber() << " Function Name = " <<  MF.getName() << "\n" );
  DEBUG( dbgs() << " -------------------------------------------------- \n" );

  auto BBIF = GI->second;

  for(auto BB = BBIF->begin(); BB != BBIF->end(); BB++)
    {

      auto vecIF =  BB->second;

      // For all Information Flows
      for(auto IF = vecIF->begin(); IF != vecIF->end(); IF++)
        {

          DEBUG(errs() << " **************************** \n");
          (*IF)->dump();
          DEBUG(errs() << " **************************** \n");

          // Size of Outs and Ins
          auto size_outs = (*IF)->Outs.size();
          auto size_ins = (*IF)->Ins.size();

          // DEBUG(errs() << " IF " << " size Outs = " << (*IF)->Outs.size()  << " size Ins = " << (*IF)->Ins.size() <<  " : \n");
          // (*IF)->dump();

          // If this is a Dynamic Information Flow
           if( (*IF)->isDynamic() )
            {
              DEBUG(errs() << " is Dynamic " << " size Outs = " << (*IF)->Outs.size()  << " size Ins = " << (*IF)->Ins.size() <<  " : \n");
              (*IF)->dump();
              DEBUG(errs() << "\n");

              DEBUG(errs() << " **************************** \n");
              (*IF)->dump();
              DEBUG(errs() << " **************************** \n");

            }

          // If this is a Static Information Flow
          if( (*IF)->isStatic() )
            {
              DEBUG(errs() << " is Static " << " size Outs = " << (*IF)->Outs.size()  << " size Ins = " << (*IF)->Ins.size() <<  " : \n");
              (*IF)->dump();
              DEBUG(errs() << "\n");

              for(auto out = (*IF)->Outs.begin(); out != (*IF)->Outs.end(); out++)
                {
                  DEBUG(errs() << " **************************** \n");
                  (*IF)->dump();
                  DEBUG(errs() << " **************************** \n");

                  if(out->isReg())
                    {
                      auto ins = (*IF)->Ins.find(*out);

                      if(ins != (*IF)->Ins.end())
                        {

                          DEBUG(errs() << "FOUND\n");

                          // Rx <- Rx
                          if( (size_outs == 1) && (size_ins == 1) )
                            {
                              DEBUG(errs() << "SIMPLIFICATION : DELETE FLOW (Rx <- Rx) :" << "\n");
                              (*IF)->dump();

                              vecIF->erase(IF);
                              IF--;
                              //  deleted = true;
                              break;

                              DEBUG(errs() << "by \n");
                            }

                          // Rx, Ry <- Rx
                          else if( (size_outs > 1) && (size_ins == 1) )
                            {
                              DEBUG(errs() << "SIMPLIFICATION : DELETE OUTS (Rx, Ry <- Rx) " << "\n");
                              (*IF)->dump();

                              (*IF)->TagOp = TAG_OP::Update;
                              (*IF)->Outs.erase(out);

                              DEBUG(errs() << "by : \n");
                              (*IF)->dump();
                            }

                          // Rx <- Rx, Ry
                          else if( (size_outs == 1) && (size_ins > 1) )
                            {
                              DEBUG(errs() << "SIMPLIFICATION : DELETE INS (Rx <- Rx, Ry) " << "\n");
                              (*IF)->dump();

                              (*IF)->TagOp = TAG_OP::Update;
                              (*IF)->Ins.erase(ins);

                              DEBUG(errs() << "by : \n");
                              (*IF)->dump();
                            }

                          // Rx, Ry <- Rx, Rz
                          else
                            {
                              DEBUG(errs() << " TODO SIMPLIFICATION :  Rx, Ry <- Rx, Rz " << "\n");
                            }

                        }
                      else
                        {
                          DEBUG(errs() << "NOT FOUND\n");
                        }
                    }
                }
            }
        }
    }

  DEBUG(errs() << " ************************************************************* \n");

}






void HardBlareAnalysis::ModifyRegsAnalysis(GlobalInformationFlows& _GlobalInformationFlows, MachineFunction &MF)
{

  const TargetRegisterInfo *TRI = MF.getSubtarget().getRegisterInfo();
  PostDominatorTree *PDT;

  DEBUG(errs() << " ************************************************************* \n");
  DEBUG(errs() << "                        modifyRegsAnalysis                     \n");
  DEBUG(errs() << " ************************************************************* \n");

  MachineDominatorTree MDT;
  MDT.runOnMachineFunction(MF);

  MachinePostDominatorTree MPT;
  MPT.runOnMachineFunction(MF);

  DEBUG(errs() << "                        MachineDominatorTree                     \n");
  DEBUG(errs() << " ************************************************************* \n");
  MDT.dump();


  DEBUG(errs() << "                        MachinePostDominatorTree               \n");
  DEBUG(errs() << " ************************************************************* \n");
  MPT.dump();


  DEBUG(errs() << "                        LiveVariables               \n");
  DEBUG(errs() << " ************************************************************* \n");


}




void HardBlareAnalysis::GenerateInstrumentation(GlobalInformationFlows& _GlobalInformationFlows, MachineFunction &MF)
{

  const ARMInstrInfo &TII = *static_cast<const ARMInstrInfo*>(MF.getTarget().getMCInstrInfo());


  auto GI = _GlobalInformationFlows.find(MF.getFunctionNumber());

  if (GI == _GlobalInformationFlows.end())
    {
      assert(false && "HardBlareAnalysis::GenerateInstrumentation NOT FOUND (GlobalInformationFlows.find)");
    }

  auto FuncIF = GI->second;

  // For all basic block
  for(auto BB = FuncIF->begin(); BB != FuncIF->end(); BB++)
    {
      auto vecIF =  BB->second;
      // For all Information Flows
      for(auto IF = vecIF->begin(); IF != vecIF->end(); IF++)
        {
          // If this is a Dynamic Information Flow
          if( (*IF)->isDynamic() )
            {

              if((*IF)->needInstrumentation())
                {

                  HardBlareDynamicInformationFlow* DIF = static_cast<HardBlareDynamicInformationFlow*>(*IF);

                  DEBUG( dbgs() << "IF = ");
                  DIF->dump();
                  DEBUG( dbgs() << "\n");

                  //  auto DIF = dynamic_cast<HardBlareDynamicInformationFlow*>(*IF);
                  DEBUG( dbgs() << " After static_cast of MI \n" );
                  HardBlareInstrumentation::sendValueToPL(DIF, TII);
                }
            }
        }
    }
}
