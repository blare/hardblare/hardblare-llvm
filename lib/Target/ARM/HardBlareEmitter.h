//===-------------------- Emitter.h - HardBlare -----------------------===//
//
//
// HardBlare Project
//
// This file describe ....
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//


#ifndef LLVM_HARDBLARE_EMITTER_H
#define LLVM_HARDBLARE_EMITTER_H


#include "llvm/CodeGen/AsmPrinter.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/MC/MCContext.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCObjectFileInfo.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOpcodes.h"
#include <iterator>

#include "HardBlareInformationFlow.h"

#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"

#define DEBUG_TYPE "hardblare-emitter"

using namespace llvm;


// Values
// ------------
#define HARDBLARE_VERSION 2
#define HARDBLARE_REPRESENTATION 2

class HardBlareEmitter {
 public:

  AsmPrinter &AP;

  // Constructor
 HardBlareEmitter(AsmPrinter &AP) : AP(AP) {}

  // Emit all annotations
  void emitAnnotations(MCStreamer &OS, GlobalInformationFlows* _GIF);

  // Emit the header
  void emitHardBlareHeader(MCStreamer &OS);

  // Emit HardBlare
  void emit(MCTargetStreamer &TS);

};


#endif
