//===---------------------------- HardBlareMCTagDepInstr.cpp ---------------------------===//
//
//
// HardBlare Project
//
// This file describe ....
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//


#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/MC/MCContext.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCObjectFileInfo.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOpcodes.h"
#include "llvm/Target/TargetSubtargetInfo.h"
#include "llvm/CodeGen/MachineOperand.h"

#include "llvm/Support/Debug.h"

#include "llvm/ADT/DenseMap.h"
#include <iterator>
#include "llvm/ADT/MapVector.h"
#include "llvm/ADT/SmallVector.h"
#include <map>
#include <vector>


#include "HardBlareInformationFlow.h"
#include "HardBlareAnalysis.h"


using namespace llvm;



// DEBUG NO INFORMATION FLOW FOR THE ARM INSTRUCTION (TODO)
// Singleton Pattern for the Global Information Flows
std::vector<unsigned> TODO_VECT;

GlobalInformationFlowsSingleton& GlobalInformationFlowsSingleton::getInstance()
{
  static GlobalInformationFlowsSingleton instance;
  DEBUG( dbgs() << " GET GlobalInformationFlowsSingleton instance = " << &instance <<  "\n" );
  return instance;
}

GlobalInformationFlows* GlobalInformationFlowsSingleton::getGlobalInformationFlows()
{
  DEBUG( dbgs() << " GET getGlobalInformationFlows GlobalInformationFlows instance = " << GIF <<  "\n" );
  return GIF;
}

GlobalInformationFlowsSingleton::~GlobalInformationFlowsSingleton()
{
  DEBUG( dbgs() << "DESTRUCT GlobalInformationFlowsSingleton \n" );
}

GlobalInformationFlowsSingleton::GlobalInformationFlowsSingleton()
{
  DEBUG( dbgs() << "CREATE GlobalInformationFlowsSingleton \n" );
  GIF = new GlobalInformationFlows();
}




// ============================================
// Helper Functions for dump
// ============================================
std::string HardBlareInformationFlowHelpers::getARMOpTypeString(ARM_OPCODE_TYPE _type)
{
  // MoveOp
  if( _type == ARM_OPCODE_TYPE::MoveOp )
    {
      return "MoveOp";
    }
  // ArithLogicOp
  else if( _type == ARM_OPCODE_TYPE::ArithLogicOp)
    {
      return "ArithLogicOp";
    }
  // CompOp
  else if( _type == ARM_OPCODE_TYPE::CompOp)
    {
      return "CompOp";
    }
  // ArithLogicCompOp
  else if( _type == ARM_OPCODE_TYPE::ArithLogicCompOp)
    {
      return "ArithLogicCompOp";
    }
  // FPMoveOp
  else if( _type == ARM_OPCODE_TYPE::FPMoveOp)
    {
      return "FPMoveOp";
    }
  // FPArithLogicOp
  else if( _type == ARM_OPCODE_TYPE::FPArithLogicOp)
    {
      return "FPArithLogicOp";
    }
  // FPCompOp
  else if( _type == ARM_OPCODE_TYPE::FPCompOp)
    {
      return "FPCompOp";
    }

  // UPDATE FP
  else if( _type == ARM_OPCODE_TYPE::LOAD_STORE)
    {
      return "LOAD_STORE";
    }

  // UPDATE_TRACKED_VALUE_REG
  else if( _type == ARM_OPCODE_TYPE::UPDATE_TRACKED_VALUE_REG)
    {
      return "UPDATE_TRACKED_VALUE_REG";
    }

  // UPDATE FP
  else if( _type == ARM_OPCODE_TYPE::MOVE_VALUE_TRACKED_REG)
    {
      return "MOVE_VALUE_TRACKED_REG";
    }

  // 
  else if( _type == ARM_OPCODE_TYPE::CALL_RET )
    {
      return "CALL_RET";
    }

  // Else, Nothing to do (System instr., etc.)
  else
    {
       return "??????";
    }
}


std::string HardBlareInformationFlowHelpers::getTagOpString(TAG_OP _op)
{
  if( _op == TAG_OP::Init)
    {
      return "Init";
    }
  else if( _op == TAG_OP::Update)
    {
      return "Update";
    }
  else if( _op == TAG_OP::Check)
    {
      return "Check";
    }
  else if( _op == TAG_OP::Init_Tag_Val)
    {
      return "Init_Tag_Val";
    }
  else if( _op == TAG_OP::None)
    {
      return "None";
    }
  else
    {
      return "??????";
    }
}


// Init Functions
// ============================================

enum ARM_OPCODE_TYPE HardBlareInformationFlowHelpers::getARMOpType(MachineBasicBlock::instr_iterator MI)
{

  auto Desc = MI->getDesc();

  // MoveOp
  if(Desc.isMoveOp())
    {
      return ARM_OPCODE_TYPE::MoveOp;
    }
  // ArithLogicOp
  else if(Desc.isArithLogicOp())
    {
      return ARM_OPCODE_TYPE::ArithLogicOp;
    }
  // CompOp
  else if(Desc.isCompOp())
    {
      return ARM_OPCODE_TYPE::CompOp;
    }
  // ArithLogicCompOp
  else if(Desc.isArithLogicCompOp())
    {
      return ARM_OPCODE_TYPE::ArithLogicCompOp;
    }
  // FPMoveOp
  else if(Desc.isFPMoveOp())
    {
      return ARM_OPCODE_TYPE::FPMoveOp;
    }
  // FPArithLogicOp
  else if(Desc.isFPArithLogicOp())
    {
      return ARM_OPCODE_TYPE::FPArithLogicOp;
    }
  // FPCompOp
  else if(Desc.isFPCompOp())
    {
      return ARM_OPCODE_TYPE::FPCompOp;
    }

  // LOAD/STORE
  else if(Desc.isSimpleLoad() ||
          Desc.isMultipleLoad() ||
          Desc.isVFPLoad() ||
          Desc.isSimpleStore() ||
          Desc.isMultipleStore() ||
          Desc.isVFPStore() ||
          (MI->getOpcode() == ARM::LDRcp) // Exception because this pseudo-instr is not treated as others LDR/STR
          )
    {
      return ARM_OPCODE_TYPE::LOAD_STORE;
    }

  // Else, Nothing to do (System instr., etc.)
  else
    {
      TODO_VECT.push_back(MI->getOpcode());
      DEBUG(errs() << "ARM_OPCODE_TYPE Not found" << "\n");
      //   assert( false && "ARM_OPCODE_TYPE Not found");
      // MI->dump();
    }
}





/*****************************************************************************

 HardBlareInformationFlow

*****************************************************************************/



// Helper Functions
// ============================================

bool HardBlareInformationFlowHelpers::needsInstrumentation(MachineBasicBlock::instr_iterator MI)
{
  auto Desc = MI->getDesc();

  if(Desc.isSimpleLoad() ||
     Desc.isMultipleLoad() ||
     Desc.isVFPLoad() ||
     Desc.isSimpleStore() ||
     Desc.isMultipleStore() ||
     Desc.isVFPStore() )
    {
      return true;
    }

  else
    {
      return false;
    }

}


bool HardBlareInformationFlowHelpers::isPC(MachineOperand& _Op)
{

  assert( _Op.isReg() && "Error isPC : _Op Not a Register");

  if( _Op.getReg() == ARM::PC)
    {
      return true;
    }

  return false;

}


bool HardBlareInformationFlowHelpers::isSP(MachineOperand& _Op)
{

  assert( _Op.isReg() && "Error isSP : _Op Not a Register");

  if( _Op.getReg() == ARM::SP)
    {
      return true;
    }

  return false;

}



bool HardBlareInformationFlowHelpers::isFP(MachineOperand& _Op)
{

  assert( _Op.isReg() && "Error isFP : _Op Not a Register");

  if( _Op.getReg() == ARM::R11)
    {
      return true;
    }

  return false;

}


bool HardBlareInformationFlowHelpers::isLR(MachineOperand& _Op)
{

  assert( _Op.isReg() && "Error isLR : _Op Not a Register");

  if( _Op.getReg() == ARM::LR)
    {
      return true;
    }

  return false;

}


bool HardBlareInformationFlowHelpers::isUpdateSP(MachineBasicBlock::instr_iterator _MI, MachineOperand& _Op)
{
  auto Desc = _MI->getDesc();
  if( (Desc.isArithLogicOp()) && (isSP(_Op)) )
    {
      return true;
    }
  return false;
}



bool HardBlareInformationFlowHelpers::isUpdateFP(MachineBasicBlock::instr_iterator _MI, MachineOperand& _Op)
{
  auto Desc = _MI->getDesc();
  if( (Desc.isArithLogicOp()) && (isFP(_Op)) )
    {
      return true;
    }
  return false;
}



MachineOperand& HardBlareInformationFlowHelpers::getRd(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Rd);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}


MachineOperand& HardBlareInformationFlowHelpers::getRn(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Rn);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;

}

MachineOperand& HardBlareInformationFlowHelpers::getRm(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Rm);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;

}


MachineOperand& HardBlareInformationFlowHelpers::getRa(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Ra);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}

MachineOperand& HardBlareInformationFlowHelpers::getRdHi(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::RdHi);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}


MachineOperand& HardBlareInformationFlowHelpers::getRdLo(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::RdLo);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;

}

MachineOperand& HardBlareInformationFlowHelpers::getRn_wb(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Rn_wb);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}

MachineOperand& HardBlareInformationFlowHelpers::getRt(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Rt);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;

}


MachineOperand& HardBlareInformationFlowHelpers::getRt2(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Rt2);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}


MachineOperand& HardBlareInformationFlowHelpers::getRegs(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::regs);
  assert( (OpIdx >= 0) && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}

MachineOperand& HardBlareInformationFlowHelpers::getRegsI(const MachineBasicBlock::instr_iterator MI, unsigned i)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::regs);
  assert( ((OpIdx >= 0) && ((OpIdx + i) < MI->getNumOperands())) && "Error: OpIdx < 0  (i < MI->getNumOperands()) ");
  MachineOperand &Op = (MI->getOperand(OpIdx+i));
  return Op;
}


MachineOperand& HardBlareInformationFlowHelpers::getBaseAddr(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::addr);
  assert( (OpIdx >= 0) && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}


MachineOperand& HardBlareInformationFlowHelpers::getOffsetRegAddr(const MachineBasicBlock::instr_iterator MI)
{
  int RegOpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::addr);
  unsigned RegOffsetOpIdx = RegOpIdx + 1;
  assert( (RegOpIdx >= 0) && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(RegOffsetOpIdx));
  return Op;
}

MachineOperand& HardBlareInformationFlowHelpers::getOffsetImmAddr(const MachineBasicBlock::instr_iterator MI)
{

  unsigned RegOpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::addr);
  unsigned OffsetOpIdx = RegOpIdx + 1;
  assert(((RegOpIdx >= 0) && (OffsetOpIdx >= 0)) && "Error: RegOpIdx < 0 OR OffsetOpIdx < 0");
  MachineOperand &OffsetOp = (MI->getOperand(OffsetOpIdx));
  assert( OffsetOp.isImm() && "Error: OffsetOp is not an Imm");
  return OffsetOp;
}


MachineOperand& HardBlareInformationFlowHelpers::getOffsetImmAddr2(const MachineBasicBlock::instr_iterator MI)
{
  unsigned RegOpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::addr);
  unsigned OffsetOpIdx = RegOpIdx + 2;
  assert(((RegOpIdx >= 0) && (OffsetOpIdx >= 0)) && "Error: RegOpIdx < 0 OR OffsetOpIdx < 0");
  MachineOperand &OffsetOp = (MI->getOperand(OffsetOpIdx));
  assert( OffsetOp.isImm() && "Error: OffsetOp is not an Imm");
  return OffsetOp;
}



MachineOperand& HardBlareInformationFlowHelpers::getShiftBase(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::shift);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}


MachineOperand& HardBlareInformationFlowHelpers::getShiftBase2(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::shift);
  OpIdx = OpIdx + 1;
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}



MachineOperand& HardBlareInformationFlowHelpers::getShiftImm(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::shift);
  int OpIdxImm = OpIdx + 2;
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdxImm));
  return Op;
}


MachineOperand& HardBlareInformationFlowHelpers::getDst(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::dst);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;

}

MachineOperand& HardBlareInformationFlowHelpers::getImm(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::imm);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}

MachineOperand& HardBlareInformationFlowHelpers::getImm16(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::imm16);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}

MachineOperand& HardBlareInformationFlowHelpers::getOffset(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::offset);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}



MachineOperand& HardBlareInformationFlowHelpers::getSd(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Sd);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}


MachineOperand& HardBlareInformationFlowHelpers::getSm(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Sm);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}


MachineOperand& HardBlareInformationFlowHelpers::getSn(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Sn);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}

MachineOperand& HardBlareInformationFlowHelpers::getDd(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Dd);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}


MachineOperand& HardBlareInformationFlowHelpers::getDm(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Dm);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}


MachineOperand& HardBlareInformationFlowHelpers::getDn(const MachineBasicBlock::instr_iterator MI)
{
  int OpIdx = ARM::getNamedOperandIdx(MI->getOpcode(), ARM::OpName::Dn);
  assert(OpIdx >= 0 && "Error: OpIdx < 0");
  MachineOperand &Op = (MI->getOperand(OpIdx));
  return Op;
}

bool HardBlareInformationFlowHelpers::mayAffectControlFlow(const MachineBasicBlock::instr_iterator MI, const MCRegisterInfo &RI)
{

  if ( MI->isBranch() || MI->isCall() || MI->isReturn() || MI->isIndirectBranch())
    return true;

  unsigned PC = RI.getProgramCounter();

  if (PC == 0)
    return false;
  /// if (hasDefOfPhysReg(MI, PC, RI))
  // return true;
  // A variadic instruction may define PC in the variable operand list.
  // There's currently no indication of which entries in a variable
  // list are defs and which are uses. While that's the case, this function
  // needs to assume they're defs in order to be conservatively correct.
  for (int i = 0, e = MI->getNumOperands(); i != e; ++i) {
    if (MI->getOperand(i).isReg() &&
        RI.isSubRegisterEq(PC, MI->getOperand(i).getReg()))
      return true;
  }
  return false;

}

void HardBlareInformationFlow::GenerateInformationFlows(MachineFunction &MF,  GlobalInformationFlows& _GlobalInformationFlows){

  // MachineFrameInfo &MFI = MF.getFrameInfo();
  const ARMInstrInfo &TII = *static_cast<const ARMInstrInfo*>(MF.getTarget().getMCInstrInfo());
  MCContext &MCContext = MF.getContext();
  const MCRegisterInfo &MRI = *MCContext.getRegisterInfo();

  FunctionInformationFlows *FuncInformationFlows = new FunctionInformationFlows();
  BasicBlockInformationFlows *BBInformationFlows;
  InformationFlows *VectorInformationFlows;
  unsigned int incrementSymbolPTM = 0;

  // Init GlobalID of Dynamic Information Flow
  //HardBlareDynamicInformationFlow::globalID = 0;

  // For all basic blocks
  for (MachineFunction::iterator MBB = MF.begin(), MBBE = MF.end(); MBB != MBBE; ++MBB)
    {

      // Creating a new vector of information flows for the basic block MBB
      VectorInformationFlows = new InformationFlows();

      // Creating a couple Information Flows <-> BasicBlock symbol
      BBInformationFlows = new BasicBlockInformationFlows();


      // If this is the first instruction of the function's Entry Basic Block
      if( (MBB == MF.begin()) )
        {
          MCSymbol *LabelBeginFunction = MCContext.getOrCreateSymbol(MF.getName());
          BBInformationFlows->first = LabelBeginFunction;
        }
      else
        {
          MCSymbol *LabelBasicBlock = MBB->getSymbol();
          BBInformationFlows->first = LabelBasicBlock;
        }


      // For all instructions
      for (MachineBasicBlock::instr_iterator MI = MBB->instr_begin(), MIE = MBB->instr_end(); MI != MIE; ++MI )
        {

          // The next instruction
          MachineBasicBlock::instr_iterator MI_next = MI;

          DEBUG( dbgs() << "\n**************************************************** \n" );
          DEBUG( dbgs() << "******************** HardBlare ********************* \n" );
          DEBUG( dbgs() << "**************************************************** \n" );
          MI->dump();
          DEBUG( dbgs() << "---------------------------------------------------- \n" );

          // Generating the Information Flow of the MI Instruction
          if(HardBlareInformationFlowHelpers::needsInstrumentation(MI) )
            {
              HARDBLARE_INSTR(
                              HardBlareDynamicInformationFlow::GenerateHardBlareDynamicInformationFlow(MI, VectorInformationFlows)
                              );
            }
          else
            {
              HARDBLARE_ANNOT(
                              HardBlareStaticInformationFlow::GenerateHardBlareStaticInformationFlow(MI, VectorInformationFlows)
                              );
            }

          DEBUG( dbgs() << "**************************************************** \n\n\n" );


          // If this instruction affect the Control Flow (e.g bl rand),
          // we need a label just after it for the return of the callee.
          if( HardBlareInformationFlowHelpers::mayAffectControlFlow(MI, MRI) )
            {
              if( (MI_next != MIE) && !(MI->isTerminator()) )
                {

                  // Create a new symbol label ".PTM_func_X"
                  MCSymbol *Label = MCContext.getOrCreateSymbol(".PTM_"
                                                                + MF.getName()
                                                                + "_"
                                                                + Twine(incrementSymbolPTM)
                                                                );
                  // Increment the symbol number
                  incrementSymbolPTM++;

                  // Create the instruction Label
                  MachineInstrBuilder MILabel = BuildMI(MF,
                                                        MI->getDebugLoc(),
                                                        TII.get(TargetOpcode::GC_LABEL));
                  // Add the symbol to the instruction
                  MILabel.addSym(Label);

                  // Insert the instruction
                  MBB->insertAfter(MI, MILabel);

                  // Push the vector of information flows
                  BBInformationFlows->second = VectorInformationFlows;

                  // Push the Basic block' information flows
                  FuncInformationFlows->push_back(*BBInformationFlows);

                  // Creating a new vector of information flows for the basic block MBB
                  VectorInformationFlows = new InformationFlows();

                  // Creating a couple Information Flows <-> BasicBlock symbol
                  BBInformationFlows = new BasicBlockInformationFlows();

                  // Next
                  BBInformationFlows->first = Label;

                  // Skip the Label instruction by incrementing the iterator
                  ++MI;
                }
            }
        }

      // Push the vector of information flows
      BBInformationFlows->second = VectorInformationFlows;

      // Push the Basic block' information flows
      FuncInformationFlows->push_back(*BBInformationFlows);

    }

  _GlobalInformationFlows.insert( {MF.getFunctionNumber(), FuncInformationFlows} );

  DEBUG( dbgs() << " \n\n\n\n\n\n\n\n TODO Information Flows : \n\n " );
  for (auto todo = TODO_VECT.begin(); todo != TODO_VECT.end(); ++todo)
    {
      DEBUG( dbgs() << *todo << "\n " );
    }

}


void HardBlareInformationFlow::dump(GlobalInformationFlows& _GlobalInformationFlows)
{

  DEBUG( dbgs() << " \n\n\n\n\n\n\n\n Information Flows dump : \n\n " );

  for(auto GI = _GlobalInformationFlows.begin(); GI != _GlobalInformationFlows.end(); GI++)
    {

      DEBUG( dbgs() << "\n\nFunction ID : " << GI->first << "\n" );
      DEBUG( dbgs() << " -------------------------------------------------- \n" );

      auto FuncIF =  GI->second;

      for(auto FIF = FuncIF->begin(); FIF != FuncIF->end(); FIF++)
        {

          DEBUG( dbgs() << "\n\nBasicBlock Name : " << FIF->first->getName() << "\n" );
          DEBUG( dbgs() << " -------------------------------------------------- \n" );

          auto BBIF =  FIF->second;

          for(auto IF = BBIF->begin(); IF != BBIF->end(); IF++)
            {
              (*IF)->dump();
            }

          DEBUG( dbgs() << " -------------------------------------------------- \n" );
          DEBUG( dbgs() << "\n\n\n" );

        }
    }
}

