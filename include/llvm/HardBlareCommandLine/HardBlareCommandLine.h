//===----------------------- HardBlareCommandLine.h -----------------------===//
//
// HardBlare Project
//
// This file HardBlareFlag.h permit to access to the '-hardblare' command
// line option
//
// Author : NASR ALLAH Mounir
//           http://nasrallah.fr
//           mounir.nasrallah@centralesupelec.fr
//
//===----------------------------------------------------------------------===//


// HardBlareFlag - This boolean is set to true if the '-hardblare' command line option
// is specified.

extern bool HardBlareAnnotFlag;
extern bool HardBlareInstrFlag;
extern bool HardBlareSelectPassFlag;


#ifdef LLVM_HARDBLARE_COMMAND_LINE_H


#define HARDBLARE_SELECT_PASS(X)

#define HARDBLARE_ANNOT(X)

#define HARDBLARE_INSTR(X)

#else

// HARDBLARE macro - This macro should be used by code to activate/desactivate
// Passes/Optimization for HardBlare.
// If the '-hardblare' option is specified on the command line
// then the code specified as the option to the macro will be
// executed.  Otherwise it will not be.
#define HARDBLARE_ANNOT(X) do { if (HardBlareAnnotFlag) { X; } } while (0)

#define HARDBLARE_INSTR(X) do { if (HardBlareInstrFlag) { X; } } while (0)

#define HARDBLARE_SELECT_PASS(X) do { if (HardBlareSelectPassFlag) { X; } } while (0)

#endif
